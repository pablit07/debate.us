# Core Django imports
from django.contrib import admin

# App-specific imports
from .models import Community, UserData


class UserDataAdmin(admin.ModelAdmin):
    model = UserData
    fields = ['user', 'date_created', 'date_last_edited']


# class CommunityAdmin(admin.ModelAdmin):
#     model = Community
#     fields = ['user', 'name']
#     readonly_fields = ['moderators', 'contributors', 'members_invited', 'members_pending']

# admin.site.register(Community, CommunityAdmin)