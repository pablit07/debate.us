# Core Django imports
from django.contrib.auth.models import Group, User
from django.contrib.contenttypes.fields import GenericRelation, GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.utils import IntegrityError

# Third party imports
from guardian.shortcuts import assign_perm


class UserData(models.Model):
    user = models.ForeignKey(User)
    # organization = models.ForeignKey(Organization, blank=True, null=True)
    # team = models.ForeignKey(Team, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_last_edited = models.DateTimeField(blank=True, null=True)
    deleted = models.BooleanField(default=False)
    # removed per 1107370
    # VALUES = (
    #     (1, "Username"),
    #     (2, "Full Name"),
    #     (3, "Pseudonym"),
    #     (4, "Anonymous"),
    # )
    # author = models.PositiveSmallIntegerField(max_length=1, choices=VALUES, default=1)

    class Meta:
        abstract = True

    @property
    def display_author(self):
        return self.user.get_full_name()
        # removed per 1107370
        # if self.author == 1:
        #     return self.user.username
        # elif self.author == 2:
        #     return self.user.get_full_name()
        # elif self.author == 3:
        #     return self.user.userprofile.pseudonym
        # elif self.author == 4:
        #     return "Anonymous"
# class UserData(models.Model):
#     user = models.ForeignKey(User)
#     date_created = models.DateTimeField(blank=True, null=True)
#     date_last_edited = models.DateTimeField(blank=True, null=True)
#     deleted = models.BooleanField(default=False)
#
#     class Meta:
#         abstract = True


class Community(UserData):
    name = models.CharField(max_length=200)
    group = models.ForeignKey(Group, related_name='community_group')
    admins = models.ForeignKey(Group, related_name='community_admins')
    moderators = models.ForeignKey(Group, related_name='community_moderators')
    contributors = models.ForeignKey(Group, related_name='community_contributors')
    members_pending = models.ForeignKey(Group, related_name='community_members_pending')
    members_invited = models.ForeignKey(Group, related_name='community_members_approved')

    class Meta:
        permissions = (
            ('admin', 'Administer'),
            ('moderate', 'Moderate'),
            ('contribute', 'Contribute'),
            ('member', 'Member'),
        )
        verbose_name_plural = 'communities'

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        """
        Automatically create requisite underpinnings for the newly created community.

        :param force_insert:
        :param force_update:
        :param using:
        :param update_fields:
        :return:
        """

        self.group = Group.objects.create(name=self.name)
        self.group.save()
        self.user.groups.add(self.group)
        self.admins = Group.objects.create(name="{0}-admins".format(self.name))
        self.admins.save()
        self.user.groups.add(self.admins)
        self.moderators = Group.objects.create(name="{0}-moderators".format(self.name))
        self.moderators.save()
        self.user.groups.add(self.moderators)
        self.contributors = Group.objects.create(name="{0}-contributors".format(self.name))
        self.contributors.save()
        self.members_pending = Group.objects.create(name="{0}-pending".format(self.name))
        self.members_pending.save()
        self.members_invited = Group.objects.create(name="{0}-invited".format(self.name))
        self.members_invited.save()
        super(Community, self).save()
        assign_perm('member', self.group, self)
        assign_perm('admin', self.admins, self)
        assign_perm('moderate', self.moderators, self)
        assign_perm('contribute', self.contributors, self)

    def approve_user(self, user=None):
        if not user:
            raise IntegrityError("You must provide a user to approve!")
        else:
            if user in self.members_pending:
                self.members_pending.user_set.remove(user)

            self.group.user_set.add(user)


class CommunityTag(models.Model):
    community = models.OneToOneField(Community)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return self.community


class CommunityData(UserData):
    communities = GenericRelation(CommunityTag)

    class Meta:
        abstract = True

    @property
    def content_type(self):
        return ContentType.objects.get_for_model(self)
