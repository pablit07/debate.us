# Core Django imports
from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save

__author__ = 'archen'


class Message(models.Model):
    subject = models.CharField(max_length=200)
    text = models.TextField()
    sender = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='sender', blank=True, null=True)
    action = models.URLField(blank=True, null=True)
    action_performed = models.BooleanField(default=False)
    read_by = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)
    date_created = models.DateTimeField()
    deleted = models.BooleanField(default=False)
    date_deleted = models.DateTimeField(blank=True, null=True)
    sent_email = models.BooleanField(default=False)

    # return a human readable pseudo-ID
    def __unicode__(self):
        return self.text[:25]


class InboxManager(models.Manager):

    @staticmethod
    def send_to_user(user_inbox, from_email=None):
        recipient_list = []
        for user in user_inbox.users.all():
            recipient_list.append(user.email)

        message_list = user_inbox.messages.all()

        for message in message_list.filter(deleted=False, sent_email=False):
            send_mail(subject=message.subject, message=message.text, from_email=from_email, recipient_list=recipient_list)
            message.sent_email = True
            message.save()


def send_email_notifications(modeladmin, request, queryset):
    for inbox in queryset:
        InboxManager.send_to_user(inbox, request.user.email)


class Inbox(models.Model):
    name = models.CharField(max_length=200)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)
    messages = models.ManyToManyField(Message, blank=True)
    deleted = models.BooleanField(default=False)
    date_deleted = models.DateTimeField(blank=True, null=True)

    # return a human readable pseudo-ID
    def __unicode__(self):
        return self.name

    @staticmethod
    def create_shared_inbox(instance, created, **kwargs):
        """
        If the user is just being created, set him up with a Public Shared inbox
        """
        if created:
            public_shared = Inbox.objects.filter(name="Public Shared", deleted=False)

            if not public_shared.exists():
                public_shared = Inbox(name="Public Shared")
                public_shared.save()
            else:
                public_shared = public_shared.get()

            public_shared.users.add(instance)
            public_shared.save()



post_save.connect(Inbox.create_shared_inbox, sender=User)