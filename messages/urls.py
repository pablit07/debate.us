# Core Django imports
from django.conf.urls import patterns, url

# App-specific imports
from messages import views

__author__ = 'archen'

urlpatterns = patterns('',
                       url(r'^messages/unread/$', views.unread, name='unread'),
                       )