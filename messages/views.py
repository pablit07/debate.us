# Core Django imports
from django.db.models.expressions import RawSQL
from django.http import JsonResponse

# Third Party imports
from django.shortcuts import render
from rest_framework import permissions, viewsets

# Application-specific imports
from .models import Inbox, Message
from .permissions import Read
from .serializers import InboxSerializer, MessageSerializer

__author__ = 'archen'


class InboxViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows inboxes to be viewed.
    """
    queryset = Inbox.objects.filter(deleted=False)
    serializer_class = InboxSerializer
    permission_classes = (permissions.IsAuthenticated, Read,)


class MessageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows messages CRUD
    """
    queryset = Message.objects.filter(deleted=False)
    serializer_class = MessageSerializer
    permission_classes = (permissions.IsAuthenticated, Read,)


def unread(request):
    inboxes = Inbox.objects.filter(users__in=request.user, deleted=False)
    unread_messages = Message.objects.filter(inbox__in=inboxes, deleted=False).exclude(read_by__in=request.user).count()

    return JsonResponse({'unread': unread_messages})


def inbox_all(request):

    inboxes = Inbox.objects.filter(deleted=False)
    messages = Message.objects.filter(inbox__in=inboxes, deleted=False)\
                              .annotate(msg_inbox_name=RawSQL('select "name" from "messaging_inbox"', ""))

    # set up messages to check if read by current user
    for message in messages:
        if request.user in message.read_by.all():
            message.read_by_user = True

    context = {
        'messages': messages,
    }

    response = render(request, 'messages/inbox.html', context)

    # set messages to read (for next time)
    for message in messages:
        message.read_by.add(request.user)

    return response