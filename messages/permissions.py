# Third party imports
from rest_framework import permissions

__author__ = 'archen'


class Read(permissions.BasePermission):
    """
    Custom permission to only allow members of an inbox to interact with messages.
    """

    def has_object_permission(self, request, view, obj):
        # Only allow read access to users that are associated with the inbox
        if request.METHOD in permissions.SAFE_METHODS:
            return request.user in obj.users

        return False
