# Third party imports
from rest_framework import serializers

# Application-specific imports
from .models import Inbox, Message

__author__ = 'archen'


class InboxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inbox


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message