# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inbox',
            name='messages',
            field=models.ManyToManyField(to='messaging.Message', blank=True),
        ),
    ]
