# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0004_message_sender'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='date_created',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 28, 0, 30, 30, 387807, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
