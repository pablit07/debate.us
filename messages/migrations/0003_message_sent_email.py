# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0002_auto_20150927_0942'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='sent_email',
            field=models.BooleanField(default=False),
        ),
    ]
