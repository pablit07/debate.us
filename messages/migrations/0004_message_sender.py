# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('messaging', '0003_message_sent_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='sender',
            field=models.OneToOneField(related_name='sender', null=True, blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
