from django.contrib import admin
from messages.models import Inbox, Message, send_email_notifications


class InboxAdmin(admin.ModelAdmin):
    fields = ['name', 'users', 'messages']
    actions = [send_email_notifications]


class MessageAdmin(admin.ModelAdmin):
    fields = ['subject', 'text', 'sender', 'sent_email', 'date_created', 'read_by', 'deleted']


admin.site.register(Inbox, InboxAdmin)
admin.site.register(Message, MessageAdmin)