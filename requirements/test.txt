# Test dependencies go here.
-r base.txt

django-jenkins==0.14.1
coverage==3.7.1
django-extensions==1.3.7
