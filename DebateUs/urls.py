from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView, RedirectView


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# slop that needs to be cleaned up after full definition

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='arguemeter/', permanent=False), name='base_view'),

    # Examples:
    # url(r'^$', 'DebateUs.views.home', name='home'),
    # url(r'^DebateUs/', include('DebateUs.foo.urls')),
    url(r'^arguemeter/', include('arguemeter.urls', namespace='arguemeter')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    (r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^oauth2/', include('provider.oauth2.urls', namespace='oauth2')),
    url(r'^accounts/signup/', RedirectView.as_view(url='/landing/', permanent=False), name='signup'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^landing/', include('landing.urls', namespace='landing')),
)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns+=patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )