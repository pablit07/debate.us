# settings/dev_paul.py
from .local import *
import os

DATABASES = {
    # 'mysql': {
    #     'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
    #     'NAME': 'debateus',                      # Or path to database file if using sqlite3.
    #     'USER': 'debateus',                      # Not used with sqlite3.
    #     'PASSWORD': 'PcbW6XA8czzmGDzJ',                  # Not used with sqlite3.
    #     'HOST': '127.0.0.1',                      # Set to empty string for localhost. Not used with sqlite3.
    #     'PORT': '8889',                      # Set to empty string for default. Not used with sqlite3.
    # },
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'test',                      # Or path to database file if using sqlite3.
        'USER': 'test',                      # Not used with sqlite3.
        'PASSWORD': 'somepasswordwithoutspecialcharacters',                  # Not used with sqlite3.
        'HOST': '192.168.59.103',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    }
}


# Additional locations of static files
STATICFILES_DIRS = (
    '/Users/paulkohlhoff/Projects/debate.us/static',
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    SITE_ROOT + "/templates",
)