# settings/qa.py
from .local import *

import dj_database_url

DATABASES = {'default': dj_database_url.parse(get_env_variable('DEBATEUS_DB'))}


def show_toolbar(request):
    if request.user and (request.user.username == "archen" or request.user.username == "paul.kohlhoff"):
        return True
    return False

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'DebateUs.settings.qa.show_toolbar',
    # Rest of config
}

TASTYPIE_DEFAULT_FORMATS = ['json', 'jsonp']