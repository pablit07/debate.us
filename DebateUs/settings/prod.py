# settings/prod.py
from .base import *

import dj_database_url
import os
from unipath import Path

DATABASES = {'default': dj_database_url.parse(get_env_variable('HEROKU_POSTGRESQL_BLACK_URL'))}


def show_toolbar(request):
    if request.user and (request.user.username == "archen" or request.user.username == "paul.kohlhoff"):
        return True
    return False

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'DebateUs.settings.prod.show_toolbar',
    # Rest of config
}

TASTYPIE_DEFAULT_FORMATS = ['json', 'jsonp']

# Production Security Settings
DEBUG = False
TEMPLATE_DEBUG = DEBUG
# SESSION_COOKIE_SECURE = True
# CSRF_COOKIE_SECURE = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

# BASE_DIR = DJANGO_ROOT
# STATIC_ROOT = os.path.join(BASE_DIR, '../staticfiles/')
# STATIC_URL = '/static/'

# STATICFILES_DIRS = (
#     os.path.join(BASE_DIR, '../static/'),
# )

BASE_DIR = Path(__file__).ancestor(3)
STATIC_ROOT = BASE_DIR.child("staticfiles")
STATICFILES_DIRS = (
    BASE_DIR.child("static"),
)

# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/

# STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

DEFAULT_FROM_EMAIL = get_env_variable('EMAIL_FROM')
SERVER_EMAIL = get_env_variable('SERVER_EMAIL')
EMAIL_HOST = get_env_variable('EMAIL_HOST')
EMAIL_HOST_PASSWORD = get_env_variable('EMAIL_HOST_PASSWORD')
EMAIL_HOST_USER = get_env_variable('EMAIL_HOST_USER')
EMAIL_PORT = get_env_variable('EMAIL_PORT')
EMAIL_USE_SSL = True

ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_AUTHENTICATION_METHOD = "username_email"
ACCOUNT_EMAIL_VERIFICATION = "mandatory"

ALLOWED_HOSTS = ["*"]
