# settings/dev_kevin.py
from .local import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'debateus',                      # Or path to database file if using sqlite3.
        'USER': 'debateus',                      # Not used with sqlite3.
        'PASSWORD': '1234ASDFzxcv!QAZ',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    }
}
