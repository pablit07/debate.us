# settings/dev_kevin.py
from .local import *

import dj_database_url
DATABASES = {'default': dj_database_url.parse(get_env_variable('DEBATEUS_DB'))}


HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': get_env_variable('DEBATEUS_ES'),
        'INDEX_NAME': 'arguemeter',
    },
}