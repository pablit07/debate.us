__author__ = 'archen'
# settings/dev_kevin.py

from .local import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'test',
        'USER': 'test',
        'PASSWORD': 'somepasswordwithoutspecialcharacters',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
