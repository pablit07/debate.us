# urls for landing app
from django.conf.urls import patterns, url
from landing import views

urlpatterns = patterns('',
                       url(r'^$', view=views.index, name='index'),
                       url(r'^add-invite/$', view=views.add_invite, name='add-invite')
                       )