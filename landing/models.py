from django.core.mail import send_mail
from django.db import models


class InviteModel(models.Model):
    email = models.TextField(max_length=100)

    def save(self, *args, **kwargs):
        send_mail(subject="Invitation request", message="{email} has requested an invite to arguemeter.".format(email=
                  self.email), from_email="noreply@arguemeter.com", recipient_list=("info@arguemeter.com",))
        super(InviteModel, self).save(*args, **kwargs)