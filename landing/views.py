from django.core.validators import validate_email
from django.forms import forms
from django.http import HttpResponseBadRequest
from django.shortcuts import render


# Create your views here.
from landing.models import InviteModel


def index(request):
    return render(request, 'landing/index.html')


def add_invite(request):
    is_human = request.POST.get('human', "")
    email = request.POST.get('email', "")

    if not is_human:
        return HttpResponseBadRequest()

    if InviteModel.objects.filter(email=email).exists():
        return render(request, 'landing/index.html',
                      {'error_message': 'An invite has already been requested with this email address.'})

    try:
        validate_email(email)
        InviteModel.objects.create(email=email)
    except forms.ValidationError:
        return render(request, 'landing/index.html', {'error_message': 'Invalid email address, please try again.'})
    return render(request, 'landing/index.html', {'success_message': True})