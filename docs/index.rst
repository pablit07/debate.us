.. argumeter documentation master file, created by
   sphinx-quickstart on Wed Jul 30 00:05:42 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to argumeter's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2

   installation.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

