DebateUs.settings package
=========================

Submodules
----------

DebateUs.settings.base module
-----------------------------

.. automodule:: DebateUs.settings.base
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.ci module
---------------------------

.. automodule:: DebateUs.settings.ci
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.dev_jon module
--------------------------------

.. automodule:: DebateUs.settings.dev_jon
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.dev_kevin module
----------------------------------

.. automodule:: DebateUs.settings.dev_kevin
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.dev_kevin_pg module
-------------------------------------

.. automodule:: DebateUs.settings.dev_kevin_pg
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.dev_paul module
---------------------------------

.. automodule:: DebateUs.settings.dev_paul
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.dev_ray module
--------------------------------

.. automodule:: DebateUs.settings.dev_ray
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.kinky module
------------------------------

.. automodule:: DebateUs.settings.kinky
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.local module
------------------------------

.. automodule:: DebateUs.settings.local
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.prod module
-----------------------------

.. automodule:: DebateUs.settings.prod
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.qa module
---------------------------

.. automodule:: DebateUs.settings.qa
    :members:
    :undoc-members:
    :show-inheritance:

DebateUs.settings.settings module
---------------------------------

.. automodule:: DebateUs.settings.settings
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: DebateUs.settings
    :members:
    :undoc-members:
    :show-inheritance:
