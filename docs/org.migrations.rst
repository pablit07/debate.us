org.migrations package
======================

Submodules
----------

org.migrations.0001_initial module
----------------------------------

.. automodule:: org.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

org.migrations.0002_auto__add_field_team_name module
----------------------------------------------------

.. automodule:: org.migrations.0002_auto__add_field_team_name
    :members:
    :undoc-members:
    :show-inheritance:

org.migrations.0003_auto__add_userprofile module
------------------------------------------------

.. automodule:: org.migrations.0003_auto__add_userprofile
    :members:
    :undoc-members:
    :show-inheritance:

org.migrations.0004_auto__del_field_userprofile_team__del_field_userprofile_organization module
-----------------------------------------------------------------------------------------------

.. automodule:: org.migrations.0004_auto__del_field_userprofile_team__del_field_userprofile_organization
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: org.migrations
    :members:
    :undoc-members:
    :show-inheritance:
