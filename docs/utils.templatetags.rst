utils.templatetags package
==========================

Submodules
----------

utils.templatetags.dictionaries module
--------------------------------------

.. automodule:: utils.templatetags.dictionaries
    :members:
    :undoc-members:
    :show-inheritance:

utils.templatetags.registration_bootstrap module
------------------------------------------------

.. automodule:: utils.templatetags.registration_bootstrap
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: utils.templatetags
    :members:
    :undoc-members:
    :show-inheritance:
