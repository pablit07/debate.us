core.raters package
===================

Submodules
----------

core.raters.weightedaverage module
----------------------------------

.. automodule:: core.raters.weightedaverage
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: core.raters
    :members:
    :undoc-members:
    :show-inheritance:
