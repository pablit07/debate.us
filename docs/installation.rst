Installation Overview
=====================

This document is designed to help a new developer (or a veteran who needs some nudges to the gray matter) collect and configure the elements necessary to create a functional environment for working on *debate.us*.

Prerequisites
=============

Python 2.7.x
------------

*Debate.us* has been designed for and using *Python 2.7.x*. To install, follow the instructions for your platform on the `Python Download Page`_, or install using your operating system's package management utility. Just make sure you do not accidentally choose Python 3.x over 2.x!

.. _Python Download Page: http://www.python.org/download

Git-SCM
-------

*Debate.us* uses the *Git-SCM* utility for change management. To install, follow the instructions for your platform on the `Git download page`_, or install using your operating system's package management utility.

.. _Git download page: http://git-scm.com/downloads

pip
---

*pip* will be used to install all Python dependencies for the *Debate.us* project. To install, follow the instructions for your platform on the `pip installation page`_, or install using your operating system's package management utility.

.. _pip installation page: http://www.pip-installer.org/en/latest/installing.html#python-os-support

virtualenv and virtualenvwrapper
--------------------------------

To effectively manage and test different packages with *Debate.us* we will use *virtualenv* paired with *virtualenvwrapper*. After intsalling pip_, use the following commands to install *virtualenv* and *virtualenvwrapper*:

.. code-block:: console

	pip install virtualenv
	pip install virtualenvwrapper

For *virtualenvwrapper* to function properly, you must set the **WORKON_HOME** environment variable, and add a source reference in your shell's profile (e.g., .bash_profile). We recommend using a hidden folder named *envs* in your home directory. On unix-like systems the command syntax will be:

.. code-block:: console

	export WORKON_HOME=~/.envs
	mkdir -p $WORKON_HOME
	source /path/to/virtualenvwrapper.sh

Remember to add the export and source lines to your shell profile.

Mac OSX:

.. code-block:: console

	echo "export WORKON_HOME=~/.envs" >> ~/.bash_profile
	echo "source /path/to/virtualenvwrapper.sh" >> ~/.bash_profile

Ubuntu/Linux:

.. code-block:: console

	echo "export WORKON_HOME=~/.envs" >> ~/.bash_profile
	echo "source /path/to/virtualenvwrapper.sh" >> ~/.bash_profile

Create two environments to begin with (you can add more if you want to test different packages). Note that when you create a virtual environment using the *mkvirtualenv* command it is automatically activated. In the following commands, we will explicitly deactivate the environment after each creation:

.. code-block:: console

	mkvirtualenv debate.us
	deactivate
	mkvirtualenv debate.us_test
	deactivate

*virtualenvwrapper* allows for quick swapping and activation of environments using the following syntax:

.. code-block:: console

	workon env_name

Tab completion is also supported, by typing *workon* [space] [tab x2], virtualenvwrapper will show all of the virtual environments installed in *~/.envs*. Remember, when you're done using a virtual environment, execute the *deactivate* command.

Postgresql
----------

*Postgresql* will be used as the database of choice for the *Debate.us* project. To install, follow the instructions for your platform on the `Postgresql installation page`_, or install using your operating system's package management utility.

.. _Postgresql installation page: https://wiki.postgresql.org/wiki/Detailed_installation_guides

After installing and configuring *Postgresql* and the root user, execute the following commands:

.. code-block:: console

	postgresql -h localhost -u root
	[enter password when prompted]
	root=# CREATE DATABASE debateus;
	root=# CREATE USER debateus WITH PASSWORD 'password';
	root=# GRANT ALL ON debateus TO debateus;
	root=# \q


For convenience, you can also use the *Postgresql docker container* after cloning the git repo (todo: insert link)

.. code-block:: console

	cd debate.us/bin
	./linux_build_database.sh

Bitbucket
---------

You will need an account on bitbucket_.

.. _bitbucket: https://bitbucket.org/

After setting up your account, you will also need permission to the bitbucket repo_ for *debate.us*.

.. _repo: https://bitbucket.org/debateusadmin/debate.us

When your permissions are set you're ready to install and configure *debate.us*!


Install and Configure debate.us
===============================

Installation
------------

The preferred install location for *debate.us* is in a projects folder in your home directory. For unix-like systems, execute the following commands (replace 'user' with your username on Bitbucket_):

.. code-block:: console

	mkdir ~/projects
	cd ~/projects
	git init
	git clone https://user@bitbucket.org/debateusadmin/debate.us.git

Configuration
-------------

From ~/projects (or the root directory where you cloned the repo) execute the following commands:

.. code-block:: console

	cd debate.us
	workon debate.us
	pip install -r requirements.txt
	workon debate.us_test
	pip install -r requirements_test.txt


This will install the dependencies for both environments to be able to run *debate.us*. Next, switch to the settings directory inside of the DebateUs main project:

.. code-block:: console

	cd DebateUs/settings

In this directory, you should see multiple settings files for the *debate.us* project. For development environments, start by copying the dev_template.py file to a new file with your name replacing 'template' in the filename (note: if a settings file already exists with your first name, please add an underscore and as many initials from your last name as needed until a unique filename is created), and add it to be tracked by git:

.. code-block:: console

	cp dev_template.py dev_yournamehere.py
	git add dev_yournamehere.py

Use your text editor of choice to modify dev_yournamehere.py and change the database settings to reflect the settings created in Postgresql_:

.. code-block:: python

	DATABASES = {
	    'default': {
	        'ENGINE': 'django.db.backends.psycopg2',
	        'NAME': 'debateus',
	        'USER': '',
	        'PASSWORD': '',
	        'HOST': '127.0.0.1',                      # Set to empty string for localhost.
	        'PORT': '5432',                      # Set to empty string for default.
	    }
	}

Save the file.

For the last bit of configuration, we need to ensure that *Django* uses your newly created settings file, and a secret key of your choosing for your local environment. We will add these settings to a postactivate hook for virtualenvwrapper so that when the environment is initialized, so to are the environment variables. Locate the postactivate hook file and open it in your favorite text editor. The hook file can be found at (example using *nano* text editor):

.. code-block:: console

    nano $WORKON_HOME/debate.us/bin/postactivate

You will also need to modify *postactivate* for *debate.us_test*

Add the following lines to the end of both *postactivate* files (note: secret key can be whatever you want, but increasing length of the string increases the security):

.. code-block:: bash

	#!/bin/bash
	# This hook is run after the virtualenv is activated.**
	export DEBATEUS_SECRET_KEY=super-long-string-text-for-my-secret-key
	export DJANGO_SETTINGS_MODULE=DebateUs.settings.dev_yournamehere

Setup
-----

If you still have a running virtual environment, reset to the base *debate.us* environment now:

.. code-block:: console

	deactivate
	workon debate.us

Navigate to the root folder of the project:

.. code-block:: console

	cd ~/projects/debate.us

Now we will initialize the database:

.. code-block:: console

	./manage.py syncdb

You will be prompted to enter administrative credentials since this is the first time you have sync'd the database, **select no**

.. code-block:: console

	./manage.py migrate

.. note::

    | The migrate command will populate your database with initial test data for quick logic checks.
    | Contact Paul or Kevin for credentials.

Test
----

To test your setup, make sure that you are running the correct virtual environment:

.. code-block:: console

	deactivate
	workon debate.us

Then execute:

.. code-block:: console

	./manage.py runserver

Open your browser of choice, and navigate to the `admin page`_. By default, the admin page should be running at:

	*http://localhost:8000/admin*

.. _admin page: http://localhost:8000/admin

Enter the credentials that you created in the syncdb step of Setup_, and navigate through the admin page to get a feel for the system.

Going Forward
=============

.. attention::

    TODO: add link to submission guidelines file

.. attention::

    TODO: add link to test writing guidelines file
