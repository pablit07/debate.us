utils package
=============

Subpackages
-----------

.. toctree::

    utils.migrations
    utils.templatetags

Submodules
----------

utils.adminwrappers module
--------------------------

.. automodule:: utils.adminwrappers
    :members:
    :undoc-members:
    :show-inheritance:

utils.models module
-------------------

.. automodule:: utils.models
    :members:
    :undoc-members:
    :show-inheritance:

utils.tests module
------------------

.. automodule:: utils.tests
    :members:
    :undoc-members:
    :show-inheritance:

utils.views module
------------------

.. automodule:: utils.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: utils
    :members:
    :undoc-members:
    :show-inheritance:
