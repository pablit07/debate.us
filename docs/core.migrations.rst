core.migrations package
=======================

Submodules
----------

core.migrations.0001_initial module
-----------------------------------

.. automodule:: core.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0002_auto__del_articlecomment__del_topiccomment module
----------------------------------------------------------------------

.. automodule:: core.migrations.0002_auto__del_articlecomment__del_topiccomment
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0003_auto__add_field_comment_organization__add_field_comment_team__add_fiel module
--------------------------------------------------------------------------------------------------

.. automodule:: core.migrations.0003_auto__add_field_comment_organization__add_field_comment_team__add_fiel
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0004_auto__chg_field_ratingcriteria_value module
----------------------------------------------------------------

.. automodule:: core.migrations.0004_auto__chg_field_ratingcriteria_value
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0005_auto__del_field_axescategorization_team__del_field_axescategorization_ module
--------------------------------------------------------------------------------------------------

.. automodule:: core.migrations.0005_auto__del_field_axescategorization_team__del_field_axescategorization_
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0006_auto__add_unique_article_article_name_topic module
-----------------------------------------------------------------------

.. automodule:: core.migrations.0006_auto__add_unique_article_article_name_topic
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0007_auto__add_field_topic_detail__add_field_topic_visibility module
------------------------------------------------------------------------------------

.. automodule:: core.migrations.0007_auto__add_field_topic_detail__add_field_topic_visibility
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0008_auto__add_field_comment_deleted__add_field_topic_deleted__del_field_ra module
--------------------------------------------------------------------------------------------------

.. automodule:: core.migrations.0008_auto__add_field_comment_deleted__add_field_topic_deleted__del_field_ra
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0009_auto__add_field_rating_owner module
--------------------------------------------------------

.. automodule:: core.migrations.0009_auto__add_field_rating_owner
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0010_auto__del_field_rating_topic module
--------------------------------------------------------

.. automodule:: core.migrations.0010_auto__del_field_rating_topic
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0011_auto module
--------------------------------

.. automodule:: core.migrations.0011_auto
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0012_auto__add_field_axescategorization_is_default module
-------------------------------------------------------------------------

.. automodule:: core.migrations.0012_auto__add_field_axescategorization_is_default
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0013_auto__add_citation module
----------------------------------------------

.. automodule:: core.migrations.0013_auto__add_citation
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0014_auto__del_unique_citation_article module
-------------------------------------------------------------

.. automodule:: core.migrations.0014_auto__del_unique_citation_article
    :members:
    :undoc-members:
    :show-inheritance:

core.migrations.0015_auto__add_field_comment_organization__add_field_comment_team__add_fiel module
--------------------------------------------------------------------------------------------------

.. automodule:: core.migrations.0015_auto__add_field_comment_organization__add_field_comment_team__add_fiel
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: core.migrations
    :members:
    :undoc-members:
    :show-inheritance:
