core package
============

Subpackages
-----------

.. toctree::

    core.migrations
    core.raters

Submodules
----------

core.admin module
-----------------

.. automodule:: core.admin
    :members:
    :undoc-members:
    :show-inheritance:

core.api module
---------------

.. automodule:: core.api
    :members:
    :undoc-members:
    :show-inheritance:

core.authentication module
--------------------------

.. automodule:: core.authentication
    :members:
    :undoc-members:
    :show-inheritance:

core.authorization module
-------------------------

.. automodule:: core.authorization
    :members:
    :undoc-members:
    :show-inheritance:

core.models module
------------------

.. automodule:: core.models
    :members:
    :undoc-members:
    :show-inheritance:

core.rater module
-----------------

.. automodule:: core.rater
    :members:
    :undoc-members:
    :show-inheritance:

core.tests module
-----------------

.. automodule:: core.tests
    :members:
    :undoc-members:
    :show-inheritance:

core.views module
-----------------

.. automodule:: core.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: core
    :members:
    :undoc-members:
    :show-inheritance:
