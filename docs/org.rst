org package
===========

Subpackages
-----------

.. toctree::

    org.migrations

Submodules
----------

org.admin module
----------------

.. automodule:: org.admin
    :members:
    :undoc-members:
    :show-inheritance:

org.models module
-----------------

.. automodule:: org.models
    :members:
    :undoc-members:
    :show-inheritance:

org.tests module
----------------

.. automodule:: org.tests
    :members:
    :undoc-members:
    :show-inheritance:

org.views module
----------------

.. automodule:: org.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: org
    :members:
    :undoc-members:
    :show-inheritance:
