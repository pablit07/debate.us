# Core Django imports
import re
from urlparse import urlparse
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models, IntegrityError

# Application specific imports
from .rater import make_rating
from utils.models import UserData


class TagCloud(models.Model):
    name = models.CharField(max_length=50)


class Topic(UserData):
    topic_name = models.CharField(max_length=200)
    detail = models.TextField(blank=True)
    article_date_last_edited = models.DateTimeField(null=True)
    is_published = models.BooleanField(default=False)
    pinned = models.BooleanField(default=False)


    # return a human readable pseudo-ID
    def __unicode__(self):
        return self.topic_name

    def get_absolute_url(self):
        return reverse('arguemeter:list-articles', kwargs={'pk': self.id})

    class Meta:
        permissions = (
            ('view_topic', 'View topic'),
            ('add_article', 'Add Article'),
        )


class Rating(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=250)
    topic = models.ManyToManyField(Topic, blank=True)
    VALUES = (
        (1, 'Least Important'),
        (2, 'Less Important'),
        (3, 'Normal'),
        (4, 'More Important'),
        (5, 'Most Important'),
    )
    weight = models.PositiveSmallIntegerField(choices=VALUES)
    default_weight = models.PositiveSmallIntegerField(choices=VALUES, default=3)
    is_default = models.BooleanField(default=False)
    owner = models.ForeignKey(User, blank=True, null=True, related_name='rating_owner')
    used_by = models.ManyToManyField(User, blank=True, related_name='rating_used_by')
    date_created = models.DateTimeField(blank=True, null=True)
    date_last_edited = models.DateTimeField(blank=True, null=True)
    deleted = models.BooleanField(default=False)
    order = models.IntegerField(default=0)

    class Meta:
        unique_together = ('name', 'owner')

    # return a human readable pseudo-ID
    def __unicode__(self):
        return self.name


class Link(models.Model):
    full_url = models.URLField(max_length=2000, primary_key=True)
    domain = models.URLField()

    def save(self, *args, **kwargs):
        if not self.full_url.startswith('http://') and not self.full_url.startswith('https://'):
            self.full_url = "https://{0}".format(self.full_url)
        parsed_uri = urlparse(self.full_url)
        self.domain = parsed_uri.netloc
        super(Link, self).save(*args, **kwargs)

    # return a human readable pseudo-ID
    def __unicode__(self):
        return self.full_url


class LinkSummary(UserData):
    title = models.CharField(max_length=200)
    summary = models.TextField()
    author = models.CharField(max_length=50)
    link = models.ForeignKey(Link, blank=True, null=True)
    topic = models.ForeignKey(Topic, blank=True, null=True)

    # return a human readable pseudo-ID
    def __unicode__(self):
        return self.title

    class Meta:
        unique_together = ("link", "topic")

    @property
    def publisher(self):
        return self.link.domain

    @property
    def url(self):
        return self.link.full_url


class Article(UserData):
    article_name = models.CharField(max_length=200)
    rating_score = models.DecimalField(max_digits=10, decimal_places=3)
    rating_algorithm = models.CharField(max_length=200)
    topic = models.ForeignKey(Topic)
    content = models.TextField()
    shared_link = models.ForeignKey(LinkSummary, blank=True, null=True)

    class Meta:
        unique_together = ("article_name", "topic")

        permissions = (
            ('view_article', 'View article'),
            ('rate_article', 'Rate article'),
            ('comment', 'Comment'),
        )

    # return a human readable pseudo-ID
    def __unicode__(self):
        return self.article_name

    @property
    def agrees(self):
        return DisAgree.objects.filter(deleted=False, article=self, value=True).count()

    @property
    def disagrees(self):
        return DisAgree.objects.filter(deleted=False, article=self, value=False).count()

    @property
    def likes(self):
        return DisLike.objects.filter(deleted=False, article=self, opinion='L').count()

    @property
    def dislikes(self):
        return DisLike.objects.filter(deleted=False, article=self, opinion='D').count()

    @property
    def summary(self):
        return self.content[:250]

    # @property
    # def communities(self):
    #     return self.topic.communities

    @property
    def is_shared(self):
        return self.shared_link is not None

    @property
    def is_video(self):
        return self.is_shared and (self.shared_link.link.domain in ['www.youtube.com', 'youtu.be'])

    @property
    def video_embed_url(self):
        if self.shared_link.link.domain == 'youtu.be':
            video_id = re.findall(r'youtu\.be/(.*)$', self.shared_link.url)[0]
        else:
            video_id = re.findall(r'\?v=(.*)$', self.shared_link.url)[0]

            # try again with alternate format
        return 'https://www.youtube.com/embed/{video_id}'.format(video_id=video_id)

    @property
    def display_author(self):
        if self.is_shared:
            return self.shared_link.author
        else:
            return super(Article, self).display_author

    def get_absolute_url(self):
        return reverse('arguemeter:detail-article', kwargs={'pk': self.id})

    # def save(self, *args, **kwargs):
    #     for community in self.communities:
    #         assign_perm('view_article', community.group, self)
    #     super(Article, self).save(*args, **kwargs)


class RatingCriteria(UserData):
    rating = models.ForeignKey(Rating)
    article = models.ForeignKey(Article)
    VALUES = (
        (1, 'One Star'),
        (2, 'Two Star'),
        (3, 'Three Star'),
        (4, 'Four Star'),
        (5, 'Five Star'),
    )
    value = models.PositiveSmallIntegerField(choices=VALUES)

    class Meta:
        unique_together = ("article", "rating", "user")

    def save(self, *args, **kwargs):
        self.article.rating_score = make_rating(self.article.id)
        self.article.save()
        super(RatingCriteria, self).save(*args, **kwargs)

    # @property
    # def communities(self):
    #     return self.article.communities

    # Return a human readable pseudo-ID
    def __unicode__(self):
        return unicode(self.article) + " " + unicode(self.user.username) + " " + unicode(self.value)


class Comment(UserData):
    comment_text = models.CharField(max_length=1000)
    parent = models.ForeignKey('Comment', blank=True, null=True)
    article = models.ForeignKey(Article, blank=True, null=True)
    topic = models.ForeignKey(Topic, blank=True, null=True)

    # return a human readable pseudo-ID
    def __unicode__(self):
        return self.comment_text

    def save(self, *args, **kwargs):
        if (self.article is None and self.parent is None and self.topic is not None) \
                or (self.article is not None and self.parent is None and self.topic is None) \
                or (self.article is None and self.parent is not None and self.topic is None):
            super(Comment, self).save(*args, **kwargs)
        else:
            raise IntegrityError

    @property
    def likes(self):
        return self.dislike_set.filter(opinion='L').count()

    @property
    def dislikes(self):
        return self.dislike_set.filter(opinion='D').count()

    # @property
    # def communities(self):
    #     if self.topic:
    #         return self.topic.communities
    #     elif self.article:
    #         return self.article.communities


class AxesCategorization(UserData):
    name = models.CharField(max_length=50)
    left_criteria = models.CharField(max_length=50)
    right_criteria = models.CharField(max_length=50)
    topic = models.ManyToManyField(Topic, blank=True)
    article = models.ManyToManyField(Article, blank=True)
    is_default = models.BooleanField(default=False)
    used_by = models.ManyToManyField(User, blank=True, related_name='axes_used_by')

    # @property
    # def communities(self):
    #     return self.article.communities

    def __unicode__(self):
        return self.name


class ReasonTag(UserData):
    name = models.CharField(max_length=50)
    topic = models.ManyToManyField(Topic, blank=True)
    article = models.ManyToManyField(Article, blank=True)
    is_default = models.BooleanField(default=False)
    used_by = models.ManyToManyField(User, blank=True, related_name='tags_used_by')

    # @property
    # def communities(self):
    #     return self.article.communities

    def __unicode__(self):
        return self.name


class DisAgree(UserData):
    article = models.ForeignKey(Article)
    value = models.BooleanField()

    class Meta:
        unique_together = ("article", "user")

    # @property
    # def communities(self):
    #     return self.article.topic.communities


class DisAgreeAxis(UserData):
    axescat = models.ForeignKey(AxesCategorization, blank=True, null=True,
                                editable=False)
    slide_value = models.SmallIntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)])

    class Meta:
        unique_together = ("axescat", "user")

    # @property
    # def communities(self):
    #     return self.axescat.article.topic.communities


class DisAgreeTag(UserData):
    tag = models.ForeignKey(ReasonTag)
    article = models.ForeignKey(Article, null=True)
    value = models.NullBooleanField()
    # set auto by model save
    score = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        # calculate value as integer and save in score
        if self.value:
            self.score = 1
        else:
            self.score = 0
        super(DisAgreeTag, self).save(*args, **kwargs)

    class Meta:
        unique_together = ("tag", "article", "user", "date_deleted")

    # @property
    # def communities(self):
    #     return self.article.topic.communities


class DisLike(UserData):
    article = models.ForeignKey(Article, blank=True, null=True, editable=False)
    comment = models.ForeignKey(Comment, blank=True, null=True, editable=False)
    OPINIONS = (
        ('L', 'Like'),
        ('D', 'Dislike'),
    )
    opinion = models.CharField(max_length=1, choices=OPINIONS)

    class Meta:
        unique_together = (("article", "user"), ("comment", "user"))

    # @property
    # def communities(self):
    #     if self.article:
    #         return self.article.communities
    #     elif self.comment:
    #         return self.comment.communities


class Citation(UserData):
    article = models.ForeignKey(Article, blank=True, null=True)
    topic = models.ForeignKey(Topic, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    link = models.URLField(blank=True, null=True)

    # @property
    # def communities(self):
    #     return self.article.communities

    def __unicode__(self):
        return "article {0}, user {1}, id {2}".format(self.article, self.user, self.id)
    
    def save(self, *args, **kwargs):
        if (self.article is None and self.topic is not None) \
                or (self.article is not None and self.topic is None):
            super(Citation, self).save(*args, **kwargs)
        else:
            raise IntegrityError


class TopicFavorite(UserData):
    topic = models.ForeignKey(Topic)

    class Meta:
        unique_together = ("topic", "user", "date_deleted")