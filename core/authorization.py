__author__ = 'archen'

# Core Django imports
from django.contrib.auth import get_user_model
from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.utils.functional import wraps

# Application specific imports
from .models import Article, Citation, Comment, Topic
from community.authorization import is_owner


class UserOwnerMixin(object):
    def dispatch(self, request, *args, **kwargs):
        user = get_object_or_404(get_user_model(), pk=request.user.id)
        if user.id is not request.user.id:
            return HttpResponseForbidden("You aren't who you say you are!")

        return super(UserOwnerMixin, self).dispatch(
            request, *args, **kwargs)


class ArticleOwnerMixin(object):
    def dispatch(self, request, *args, **kwargs):
        article = get_object_or_404(Article, pk=kwargs['pk'])
        if article.user_id is not request.user.id:
            return HttpResponseForbidden("You don't own this article!")

        return super(ArticleOwnerMixin, self).dispatch(
            request, *args, **kwargs)


class TopicOwnerMixin(object):
    def dispatch(self, request, *args, **kwargs):
        topic = get_object_or_404(Topic, pk=kwargs['pk'])
        if topic.user_id is not request.user.id:
            return HttpResponseForbidden("You don't own this topic!")

        return super(TopicOwnerMixin, self).dispatch(
            request, *args, **kwargs)


def is_topic_owner(view):
    wraps(view)

    def decorator(request, pk, *args, **kwargs):
        topic = get_object_or_404(Topic, pk=pk)

        if is_owner(request, topic):
            return view(request, pk, *args, **kwargs)
        else:
            return HttpResponseForbidden("You don't own this topic!")

    return decorator


def is_not_topic_owner(view):
    wraps(view)

    def decorator(request, pk, *args, **kwargs):
        topic = get_object_or_404(Topic, pk=pk)

        if is_owner(request, topic):
            return HttpResponseForbidden("You can't argue your own topic!")
        else:
            return view(request, pk, *args, **kwargs)

    return decorator


def is_article_owner(view):
    wraps(view)

    def decorator(request, pk, *args, **kwargs):
        article = get_object_or_404(Article, pk=pk)

        if is_owner(request, article):
            return view(request, pk, *args, **kwargs)
        else:
            return HttpResponseForbidden("You don't own this argument!")

    return decorator


def is_not_article_owner(view):
    wraps(view)

    def decorator(request, pk, *args, **kwargs):
        article = get_object_or_404(Article, pk=pk)

        if is_owner(request, article):
            context = {
                'error_message': 'You can\'t do that when you own the argument!',
            }
            return view(request, pk, context, *args, **kwargs)
        else:
            return view(request, pk, *args, **kwargs)

    return decorator


def is_citation_owner(view):
    wraps(view)

    def decorator(request, citation_id, *args, **kwargs):
        citation = get_object_or_404(Citation, pk=citation_id)

        if is_owner(request, citation):
            return view
        else:
            return HttpResponseForbidden("You don't own this citation!")

    return decorator


def is_comment_owner(view):
    wraps(view)

    def decorator(request, comment_id, *args, **kwargs):
        comment = get_object_or_404(Comment, pk=comment_id)

        if is_owner(request, comment):
            return view
        else:
            return HttpResponseForbidden("You don't own this comment!")

    return decorator
