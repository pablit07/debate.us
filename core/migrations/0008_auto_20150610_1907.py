# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20150607_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='disagreetag',
            name='article',
            field=models.ForeignKey(to='core.Article', null=True),
        ),
        migrations.AlterField(
            model_name='disagreetag',
            name='value',
            field=models.NullBooleanField(),
        ),
        migrations.AlterUniqueTogether(
            name='disagreetag',
            unique_together=set([('tag', 'article', 'user')]),
        ),
    ]
