# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('article_name', models.CharField(max_length=200)),
                ('rating_score', models.DecimalField(max_digits=10, decimal_places=3)),
                ('rating_algorithm', models.CharField(max_length=200)),
                ('content', models.TextField()),
            ],
            options={
                'permissions': (('view_article', 'View article'), ('rate_article', 'Rate article'), ('comment', 'Comment')),
            },
        ),
        migrations.CreateModel(
            name='AxesCategorization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=50)),
                ('left_criteria', models.CharField(max_length=50)),
                ('right_criteria', models.CharField(max_length=50)),
                ('is_default', models.BooleanField(default=False)),
                ('article', models.ManyToManyField(to='core.Article', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Citation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('text', models.TextField(null=True, blank=True)),
                ('link', models.URLField(null=True, blank=True)),
                ('article', models.ForeignKey(blank=True, to='core.Article', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('comment_text', models.CharField(max_length=1000)),
                ('article', models.ForeignKey(blank=True, to='core.Article', null=True)),
                ('parent', models.ForeignKey(blank=True, to='core.Comment', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DisAgree',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('value', models.BooleanField()),
                ('article', models.ForeignKey(to='core.Article')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DisAgreeAxis',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('slide_value', models.SmallIntegerField(validators=[django.core.validators.MaxValueValidator(10), django.core.validators.MinValueValidator(0)])),
                ('axescat', models.ForeignKey(blank=True, editable=False, to='core.AxesCategorization', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DisLike',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('opinion', models.CharField(max_length=1, choices=[(b'L', b'Like'), (b'D', b'Dislike')])),
                ('article', models.ForeignKey(blank=True, editable=False, to='core.Article', null=True)),
                ('comment', models.ForeignKey(blank=True, editable=False, to='core.Comment', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Link',
            fields=[
                ('full_url', models.URLField(max_length=2000, serialize=False, primary_key=True)),
                ('domain', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='LinkSummary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=200)),
                ('summary', models.TextField()),
                ('author', models.CharField(max_length=50)),
                ('link', models.ForeignKey(blank=True, to='core.Link', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=250)),
                ('weight', models.PositiveSmallIntegerField(choices=[(1, b'Least Important'), (2, b'Less Important'), (3, b'Normal'), (4, b'More Important'), (5, b'Most Important')])),
                ('default_weight', models.PositiveSmallIntegerField(default=3, choices=[(1, b'Least Important'), (2, b'Less Important'), (3, b'Normal'), (4, b'More Important'), (5, b'Most Important')])),
                ('is_default', models.BooleanField(default=False)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('owner', models.ForeignKey(related_name='rating_owner', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='RatingCriteria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('value', models.PositiveSmallIntegerField(choices=[(1, b'One Star'), (2, b'Two Star'), (3, b'Three Star'), (4, b'Four Star'), (5, b'Five Star')])),
                ('article', models.ForeignKey(to='core.Article')),
                ('rating', models.ForeignKey(to='core.Rating')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='TagCloud',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('topic_name', models.CharField(max_length=200)),
                ('detail', models.TextField(blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'permissions': (('view_topic', 'View topic'), ('add_article', 'Add Article')),
            },
        ),
        migrations.AddField(
            model_name='rating',
            name='topic',
            field=models.ManyToManyField(to='core.Topic', blank=True),
        ),
        migrations.AddField(
            model_name='rating',
            name='used_by',
            field=models.ManyToManyField(related_name='rating_used_by', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AddField(
            model_name='linksummary',
            name='topic',
            field=models.ForeignKey(blank=True, to='core.Topic', null=True),
        ),
        migrations.AddField(
            model_name='linksummary',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='comment',
            name='topic',
            field=models.ForeignKey(blank=True, to='core.Topic', null=True),
        ),
        migrations.AddField(
            model_name='comment',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='citation',
            name='topic',
            field=models.ForeignKey(blank=True, to='core.Topic', null=True),
        ),
        migrations.AddField(
            model_name='citation',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='axescategorization',
            name='topic',
            field=models.ManyToManyField(to='core.Topic', blank=True),
        ),
        migrations.AddField(
            model_name='axescategorization',
            name='used_by',
            field=models.ManyToManyField(related_name='axes_used_by', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AddField(
            model_name='axescategorization',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='article',
            name='shared_link',
            field=models.ForeignKey(blank=True, to='core.LinkSummary', null=True),
        ),
        migrations.AddField(
            model_name='article',
            name='topic',
            field=models.ForeignKey(to='core.Topic'),
        ),
        migrations.AddField(
            model_name='article',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='ratingcriteria',
            unique_together=set([('article', 'rating', 'user')]),
        ),
        migrations.AlterUniqueTogether(
            name='linksummary',
            unique_together=set([('link', 'topic')]),
        ),
        migrations.AlterUniqueTogether(
            name='dislike',
            unique_together=set([('article', 'user'), ('comment', 'user')]),
        ),
        migrations.AlterUniqueTogether(
            name='disagreeaxis',
            unique_together=set([('axescat', 'user')]),
        ),
        migrations.AlterUniqueTogether(
            name='disagree',
            unique_together=set([('article', 'user')]),
        ),
        migrations.AlterUniqueTogether(
            name='article',
            unique_together=set([('article_name', 'topic')]),
        ),
    ]
