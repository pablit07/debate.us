# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_topic_article_date_last_edited'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='is_published',
            field=models.BooleanField(default=False),
        ),
    ]
