# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_disagreetag_score'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='disagreetag',
            unique_together=set([]),
        ),
    ]
