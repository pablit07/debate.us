# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_topicfavorite'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='pinned',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterUniqueTogether(
            name='topicfavorite',
            unique_together=set([('topic', 'user', 'date_deleted')]),
        ),
    ]
