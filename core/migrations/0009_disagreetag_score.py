# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20150610_1907'),
    ]

    operations = [
        migrations.AddField(
            model_name='disagreetag',
            name='score',
            field=models.IntegerField(default=0),
        ),
    ]
