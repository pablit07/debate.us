# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0004_auto_20150603_0840'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReasonTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, blank=True)),
                ('date_last_edited', models.DateTimeField(null=True, blank=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=50)),
                ('is_default', models.BooleanField(default=False)),
                ('article', models.ManyToManyField(to='core.Article', blank=True)),
                ('topic', models.ManyToManyField(to='core.Topic', blank=True)),
                ('used_by', models.ManyToManyField(related_name='tags_used_by', to=settings.AUTH_USER_MODEL, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
