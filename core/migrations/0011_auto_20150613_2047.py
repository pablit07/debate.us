# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20150613_1936'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='axescategorization',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='citation',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='comment',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='disagree',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='disagreeaxis',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='disagreetag',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='dislike',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='linksummary',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='ratingcriteria',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reasontag',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='topic',
            name='date_deleted',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='disagreetag',
            unique_together=set([('tag', 'article', 'user', 'date_deleted')]),
        ),
    ]
