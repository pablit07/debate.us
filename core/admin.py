from django.contrib import admin
# import objects here that you wish to have in the admin interface
from core.models import Article, Topic, Rating, RatingCriteria, Comment, \
    AxesCategorization, DisAgree, DisAgreeAxis, DisLike, Citation, LinkSummary, Link, ReasonTag, DisAgreeTag, \
    TopicFavorite
from community.admin import UserDataAdmin


class ArticleAdmin(UserDataAdmin):
    model = Article
    fields = UserDataAdmin.fields + ['article_name', 'rating_score', 'rating_algorithm', 'topic', 'content',
                                     'shared_link']


class TopicAdmin(UserDataAdmin):
    model = Topic
    fields = UserDataAdmin.fields + ['topic_name', 'detail', 'pinned']


class RatingCriteriaAdmin(UserDataAdmin):
    model = RatingCriteria
    fields = UserDataAdmin.fields + ['rating', 'article', 'value']


class AxesCategorizationAdmin(UserDataAdmin):
    model = AxesCategorization
    fields = UserDataAdmin.fields + ['name', 'left_criteria', 'right_criteria', 'is_default', 'topic', 'article',
                                     'used_by']


class ReasonTagAdmin(UserDataAdmin):
    model = ReasonTag
    fields = UserDataAdmin.fields + ['name', 'topic', 'article', 'used_by']


class DisAgreeAdmin(UserDataAdmin):
    model = DisAgree
    fields = UserDataAdmin.fields + ['article', 'value']


class DisAgreeAxisAdmin(UserDataAdmin):
    model = DisAgreeAxis
    fields = UserDataAdmin.fields + ['axescat', 'slide_value']


class DisAgreeTagAdmin(UserDataAdmin):
    model = DisAgreeTag
    fields = UserDataAdmin.fields + ['tag', 'value']


class DisLikeAdmin(UserDataAdmin):
    model = DisLike
    fields = UserDataAdmin.fields + ['article', 'comment', 'opinion']


class CommentAdmin(UserDataAdmin):
    model = Comment
    fields = UserDataAdmin.fields + ['comment_text', 'parent', 'article', 'topic']


class CitationAdmin(UserDataAdmin):
    model = Citation
    fields = UserDataAdmin.fields + ['article', 'topic', 'text', 'link']


class LinkAdmin(admin.ModelAdmin):
    model = Link
    fields = ['full_url']


class LinkSummaryAdmin(UserDataAdmin):
    model = LinkSummary
    fields = UserDataAdmin.fields + ['title', 'author', 'summary', 'link', 'topic']


class TopicFavoriteAdmin(UserDataAdmin):
    model = TopicFavorite
    fields = UserDataAdmin.fields + ['topic']


admin.site.register(Article, ArticleAdmin)
admin.site.register(Topic, TopicAdmin)
admin.site.register(Rating)
admin.site.register(RatingCriteria, RatingCriteriaAdmin)
admin.site.register(AxesCategorization, AxesCategorizationAdmin)
admin.site.register(DisAgree, DisAgreeAdmin)
admin.site.register(DisAgreeAxis, DisAgreeAxisAdmin)
admin.site.register(DisLike, DisLikeAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Citation, CitationAdmin)
admin.site.register(Link, LinkAdmin)
admin.site.register(LinkSummary, LinkSummaryAdmin)
admin.site.register(ReasonTag, ReasonTagAdmin)
admin.site.register(DisAgreeTag, DisAgreeTagAdmin)
admin.site.register(TopicFavorite, TopicFavoriteAdmin)