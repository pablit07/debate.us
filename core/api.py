from django.contrib.auth.models import User
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.utils import trailing_slash
from tastypie.authorization import DjangoAuthorization
from tastypie.serializers import Serializer
from authentication import OAuth20Authentication
from django.conf.urls import url
from models import Article, Topic, Rating, Comment, TagCloud, AxesCategorization, DisAgree, DisAgreeAxis, DisLike, \
    RatingCriteria


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get']
        serializer = Serializer(formats=['json', 'jsonp'])


class RatingResource(ModelResource):
    class Meta:
        queryset = Rating.objects.filter(deleted=False)
        resource_name = 'rating'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'patch', 'put', 'post']


class TopicResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')

    class Meta:
        queryset = Topic.objects.filter(deleted=False)
        resource_name = 'topic'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'patch', 'put', 'post']


class ArticleResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')
    rating = fields.ForeignKey(RatingResource, 'rating')
    topic = fields.ForeignKey(TopicResource, 'topic')

    class Meta:
        queryset = Article.objects.filter(deleted=False)
        resource_name = 'article'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'patch', 'put', 'post']


class TagCloudResource(ModelResource):
    class Meta:
        queryset = TagCloud.objects.all()
        resource_name = 'tagcloud'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'patch', 'put', 'post']


class CommentResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')
    comment_text = fields.CharField('comment_text')
    parent = fields.ToOneField('core.api.CommentResource', 'parent', null=True)
    article = fields.ToOneField('core.api.ArticleResource', 'article', null=True)
    topic = fields.ToOneField('core.api.TopicResource', 'topic', null=True)

    class Meta:
        queryset = Comment.objects.filter(deleted=False)
        resource_name = 'comment'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'patch', 'put', 'post']


class CommentViewResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')
    comment_text = fields.CharField('comment_text')
    comments = fields.ToManyField('core.api.CommentViewResource', 'comment_set', full=True)
    parent = fields.ToOneField('core.api.CommentResource', 'parent', null=True)
    article = fields.ToOneField('core.api.ArticleResource', 'article', null=True)
    topic = fields.ToOneField('core.api.TopicResource', 'topic', null=True)

    class Meta:
        queryset = Comment.objects.filter(deleted=False)
        resource_name = 'comment_view'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get']


class AxesCategorizationResource(ModelResource):
    class Meta:
        queryset = AxesCategorization.objects.filter(deleted=False)
        resource_name = 'axescategorization'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'patch', 'put', 'post']


class DisAgreeResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')

    class Meta:
        queryset = DisAgree.objects.filter(deleted=False)
        resource_name = 'disagree'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'patch', 'put', 'post']


class DisAgreeAxisResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')

    class Meta:
        queryset = DisAgreeAxis.objects.filter(deleted=False)
        resource_name = 'disagreeaxis'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'patch', 'put', 'post']


class DisLikeResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')

    class Meta:
        queryset = DisLike.objects.filter(deleted=False)
        resource_name = 'dislike'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get', 'patch', 'put', 'post']


class TopicViewResource(ModelResource):
    comments = fields.ToManyField('core.api.CommentViewResource', 'comment_set', full=True)
    articles = fields.ToManyField('core.api.ArticleViewResource', 'article_set', full=True)

    class Meta:
        queryset = Topic.objects.filter(deleted=False)
        resource_name = 'topic_view'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get']


class ArticleViewResource(ModelResource):
    comments = fields.ToManyField('core.api.CommentViewResource', 'comment_set', full=True)
    dislikes = fields.ToManyField('core.api.DisLikeResource', 'dislike_set', full=True)
    disagrees = fields.ToManyField('core.api.DisAgreeResource', 'disagree_set', full=True)

    class Meta:
        queryset = Article.objects.filter(deleted=False)
        resource_name = 'article_view'
        authentication = OAuth20Authentication()
        authorization = DjangoAuthorization()
        allowed_methods = ['get']


class TopicTestResource(ModelResource):
    articles = fields.ToManyField('core.api.ArticleResource', 'articles')

    class Meta:
        queryset = Topic.objects.filter(deleted=False)
        resource_name = 'topic_test'
        allowed_methods = ['get']

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/articles%s$" %
                (self._meta.resource_name, trailing_slash()), self.wrap_view('get_children'), name="api_get_children"),
        ]

    def get_children(self, request, **kwargs):
        article_resource = ArticleViewResource()
        return article_resource.dispatch('list', request, **kwargs)


class RatingCriteriaResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')
    article = fields.ForeignKey(ArticleResource, 'article')

    class Meta:
        queryset = RatingCriteria.objects.filter(deleted=False)
        resource_name = 'ratingcriteria'
        allowed_methods = ['get']
        filtering = {
            'user': ('exact',),
            'article': ('exact',),
        }
