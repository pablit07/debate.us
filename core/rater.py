__author__ = 'archen'

from django.dispatch import receiver
from django.conf import settings
from django.test.signals import setting_changed
from django.core.exceptions import ImproperlyConfigured
from django.utils.module_loading import import_by_path
from django.utils.crypto import get_random_string
from django.utils import importlib


UNUSABLE_RATING_PREFIX = '!'  # This will never be a valid rating
UNUSABLE_RATING_SUFFIX_LENGTH = 40  # number of random chars to add after UNUSABLE_RATING_PREFIX
RATERS = None  # lazily loaded from RATERS
PREFERRED_RATER = None  # defaults to first item in RATERS


@receiver(setting_changed)
def reset_raters(**kwargs):
    if kwargs['setting'] == 'RATERS':
        global RATERS, PREFERRED_RATER
        RATERS = None
        PREFERRED_RATER = None


def is_rating_usable(rater):
    if rater is None or rater.startswith(UNUSABLE_RATING_PREFIX):
        return False
    try:
        identify_rater(rater)
    except ValueError:
        return False
    return True


def make_rating(article, rater='default'):
    """
    Apply a score to the rating associated with an article

    """

    if article is None:
        return UNUSABLE_RATING_PREFIX + get_random_string(UNUSABLE_RATING_SUFFIX_LENGTH)

    rater = get_rater(rater)

    return rater.rate(article)


def load_raters(passed_raters=None):
    global RATERS
    global PREFERRED_RATER
    raters = []
    if not passed_raters:
        passed_raters = settings.RATERS
    for backend in passed_raters:
        rater = import_by_path(backend)()
        if not getattr(rater, 'algorithm'):
            raise ImproperlyConfigured("rater doesn't specify an "
                                       "algorithm name: %s" % backend)
        raters.append(rater)
    RATERS = dict([(rater.algorithm, rater) for rater in raters])
    PREFERRED_RATER = raters[0]


def get_rater(algorithm='default'):
    """
    Returns an instance of a loaded rater.

    If algorithm is 'default', the default rater will be returned.
    This function will also lazy import raters specified in your
    settings file if needed.
    """
    if hasattr(algorithm, 'algorithm'):
        return algorithm

    elif algorithm == 'default':
        if PREFERRED_RATER is None:
            load_raters()
        return PREFERRED_RATER
    else:
        if RATERS is None:
            load_raters()
        if algorithm not in RATERS:
            raise ValueError("Unknown rating algorithm '%s'. "
                             "Did you specify it in the RATERS "
                             "setting?" % algorithm)
        return RATERS[algorithm]


def identify_rater(article):
    """
    Returns an instance of a loaded rater.

    Identifies rating algorithm by examining rating, and calls
    get_rater() to return rater. Raises ValueError if
    algorithm cannot be identified, or if rater is not loaded.
    """
    algorithm = getattr(article, 'rating_algorithm')
    return get_rater(algorithm)


class BaseArticleRater(object):
    """
    Abstract base class for article raters

    When creating your own rater, you need to override algorithm, and rate()

    ArticleRater objects are immutable.
    """
    algorithm = None
    library = None

    def _load_library(self):
        if self.library is not None:
            if isinstance(self.library, (tuple, list)):
                name, mod_path = self.library
            else:
                name = mod_path = self.library
            try:
                module = importlib.import_module(mod_path)
            except ImportError as e:
                raise ValueError("Couldn't load %r algorithm library: %s" %
                                 (self.__class__.__name__, e))
            return module
        raise ValueError("Rater %r doesn't specify a library attribute" %
                         self.__class__.__name__)

    def rate(self, article):
        """
        Creates a rated database value

        """
        raise NotImplementedError()

    def must_update(self, article):
        return article.rating_algorithm != self.algorithm