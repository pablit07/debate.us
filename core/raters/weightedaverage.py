__author__ = 'archen'

from django.db.models import Sum
from core.rater import BaseArticleRater
from core.models import Article


class WeightedAverage(BaseArticleRater):
    algorithm = 'weighted_avg'

    def rate(self, article):
        """
        Calculates the weighted average of an article's rating criteria.

        """
        rated_article = Article.objects.get(pk=article)
        user_ratings = rated_article.ratingcriteria_set.all()

        if user_ratings:
            return (5.0*int(((user_ratings.filter(rating__weight='5').aggregate(Sum('value')))['value__sum']) or 0)
                    + 4.0*int(((user_ratings.filter(rating__weight='4').aggregate(Sum('value')))['value__sum']) or 0)
                    + 3.0*int(((user_ratings.filter(rating__weight='3').aggregate(Sum('value')))['value__sum']) or 0)
                    + 2.0*int(((user_ratings.filter(rating__weight='2').aggregate(Sum('value')))['value__sum']) or 0)
                    + int(((user_ratings.filter(rating__weight='1').aggregate(Sum('value')))['value__sum']) or 0))\
                / (5.0*(user_ratings.filter(rating__weight='5').count())
                   + 4.0*(user_ratings.filter(rating__weight='4').count())
                   + 3.0*(user_ratings.filter(rating__weight='3').count())
                   + 2.0*(user_ratings.filter(rating__weight='2').count())
                   + (user_ratings.filter(rating__weight='1').count()))
        else:
            return 1