#!/bin/bash

install_docker () {
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
    sudo sh -c "echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
    sudo apt-get update
	sudo apt-get install lxc-docker curl
	sudo sed -i '$acomplete -F _docker docker' /etc/bash_completion.d/docker.io
}

install_registry () {
	docker run -d --name registry -v /data/registry:/tmp/registry -p 5000:5000 registry
}

get_baseimage () {
	curl -H "Accept: application/vnd.github.raw" "https://api.github.com/repos/archen/docker-cookbooks/contents/baseimage/Dockerfile" -O
    docker build -t localhost:5000/baseimage .
    rm Dockerfile
}

install_pgsql () {
    docker images | grep -q "localhost:5000/baseimage" || get_baseimage
    git clone https://github.com/archen/docker-cookbooks.git
    docker build -t localhost:5000/pgsql docker-cookbooks/psql
    rm -rf docker-cookbooks
}

docker version | grep -q Client || install_docker
docker ps | grep -q "registry" || install_registry
docker images | grep -q "localhost:5000/pgsql" || install_pgsql
docker ps | grep -q "localhost:5000/pgsql" || docker run -d --name pgsql -p 5432:5432 -e POSTGRESQL_USER=test -e POSTGRESQL_PASS=somepasswordwithoutspecialcharacters -e POSTGRESQL_DB=test localhost:5000/pgsql
