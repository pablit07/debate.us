#!/bin/bash

install_homebrew () {
	ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
}

install_virtualbox () {
	brew update
	brew tap phinze/homebrew-cask
	brew install brew-cask
	brew cask install virtualbox
}

install_docker () {
	brew -v | grep -q Homebrew || install_homebrew
	virtualbox -h | grep -q Oracle || install_virtualbox

	brew update
	brew install boot2docker
	boot2docker init
	boot2docker up
}

install_registry () {
	docker run -d --name registry -v /data/registry:/tmp/registry -p 5000:5000 registry
}

init_boot2docker () {
	boot2docker init || grep -q "Creating VM boot2docker-vm" || install_docker
	boot2docker up
}

run_boot2docker() {
    boot2docker up | grep -q 'Waiting for VM' || init_boot2docker
}

get_baseimage () {
	curl -H "Accept: application/vnd.github.raw" "https://api.github.com/repos/archen/docker-cookbooks/contents/baseimage/Dockerfile" -O
    docker build -t localhost:5000/baseimage .
    rm Dockerfile
}

install_pgsql () {
    docker images | grep -q "localhost:5000/baseimage" || get_baseimage
    git clone https://github.com/archen/docker-cookbooks.git
    docker build -t localhost:5000/pgsql docker-cookbooks/psql
    docker run -d --name pgsql -p 5432:5432 -e POSTGRESQL_USER=test -e POSTGRESQL_PASS=somepasswordwithoutspecialcharacters -e POSTGRESQL_DB=test localhost:5000/pgsql
    rm -rf docker-cookbooks
}

set_docker_env () {
	# replace with conditional for sed if an old IP exists, echo if not
	export DOCKER_HOST=tcp://$(boot2docker ip 2>/dev/null):2375
	echo "export DOCKER_HOST=tcp://$(boot2docker ip 2>/dev/null):2375" >> ~/.bash_profile
}

docker version | grep -q Client || run_boot2docker
echo $DOCKER_HOST | grep -q "DOCKER_HOST=tcp://$(boot2docker ip 2>/dev/null):2375" || set_docker_env
docker ps | grep -q "registry" || install_registry
docker ps | grep -q "pgsql" || install_pgsql

cat ~/.bash_profile | grep DOCKER_HOST=tcp://$(boot2docker ip 2>/dev/null):2375 || set_docker_env
cat ~/.bash_profile | grep "alias bup='boot2docker up'" || echo "alias bup=\'boot2docker up\'" >> ~/.bash_profile
cat ~/.bash_profile | grep "alias binit='boot2docker init'" || echo "alias binit=\'boot2docker init\'" >> ~/.bash_profile
cat ~/.bash_profile | grep "alias bsus='boot2docker suspend'" || echo "alias bsus=\'boot2docker suspend\'" >> ~/.bash_profile
cat ~/.bash_profile | grep "alias bip='boot2docker ip'" || echo "alias bip=\'boot2docker ip\'" >> ~/.bash_profile
