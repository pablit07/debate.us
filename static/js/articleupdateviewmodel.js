/**
 * Created by paulkohlhoff on 6/21/15.
 */



//article update knockout viewmodel
$(function () {

    var ArticleUpdateViewModel = function () {
        var self = this;

        this.id = ko.observable();
        this.topic_id = ko.observable();
        this.article_name = ko.observable("");
        this.content = ko.observable("");
        // shared
        this.shared_link_id = ko.observable("");
        this.shared_link_url = ko.observable("");
        this.shared_link_author = ko.observable("");
        this.shared_link_title = ko.observable("");
        this.shared_link_publisher = ko.observable("");
        // collections
        this.tags = ko.observableArray([]);
        this.citations = ko.observableArray([]);

        this.unusedTags = ko.observable([]);

        this.tagToAdd = ko.observable("");
        this.citationLinkToAdd = ko.observable("");
        this.citationTextToAdd = ko.observable("");

        this.url = ko.computed(function()
        {
            return self.id() ? "/arguemeter/articles/" + self.id() + "/edit"
                : "/arguemeter/topics/" + self.topic_id() + "/articles/add/quick/"
        });

        this.deleteUrl = ko.computed(function() { return "/arguemeter/articles/" + self.id() + "/delete"; });
        // #96152072
        // enforce 5 tag limit
        this.canAddTag = ko.computed(function() { return (!!self.tagToAdd() && self.tags().length < 5); });

        // do update
        this.init = function (data, e) {
            var el = $(e.currentTarget),
                article_id = el.data('articleid'),
                topic_id = el.data('topicid'),
                new_shared = el.data('newshared');

            self.id(article_id);
            self.topic_id(topic_id);

            $('.shared-link-update').hide();
            $('.article-content-update').hide();

            if (article_id) {
                $.get("/arguemeter/api/article/" + self.id() + "/", self.updateArticle).then(function () {
                    $.get("/arguemeter/api/tags/?-article__id=" + self.id() + '&topic__id=' + self.topic_id(), self.updateUnusedTags);
                });
                $.get("/arguemeter/api/tags/?article__id=" + self.id(), self.updateTags);
                $.get("/arguemeter/api/citations/?article__id=" + self.id(), self.updateCitations);
            } else {
                if (new_shared) $('.shared-link-update').show();
                else $('.article-content-update').show();

                $.get("/arguemeter/api/tags/?topic__id=" + self.topic_id(), self.updateUnusedTags);

                var editor = tinymce.get('id_update_content');

                self.article_name("");
                editor.setContent("");
                self.content("");
                self.shared_link_id("");
                self.shared_link_url("");
                self.shared_link_author("");
                self.tags([]);
                self.citations([]);
                self.unusedTags([]);
                self.tagToAdd("");
                self.citationLinkToAdd("");
                self.citationTextToAdd("");
                self.shared_link_title("");
                self.shared_link_title.valueHasMutated();   //force clear anything from embedly
                self.shared_link_publisher("");
                self.shared_link_publisher.valueHasMutated();   //force clear anything from embedly

                editor.focus();
            }
        };

        // updates topic model from api call
        this.updateArticle = function(data) {
            self.article_name(data.article_name);
            self.topic_id(data.topic);

            var editor = tinymce.get('id_update_content');
            var content = data.content;

            // handle shared article
            if (data.shared_link) {
                self.shared_link_id(data.shared_link.id);
                self.shared_link_author(data.shared_link.author);
                self.shared_link_url(data.shared_link.link);
                self.shared_link_title(data.shared_link.title);
                self.shared_link_publisher(data.shared_link.publisher);
                $('.shared-link-update').show();
                content = data.shared_link.summary;
                editor = tinymce.get('id_summary');
            } else {
                $('.article-content-update').show();
            }

            // update the content
            // http://stackoverflow.com/questions/8505776/how-to-dynamically-replace-content-in-tinymce
            editor.setContent(content);
            editor.focus();
        };

        // update tags
        this.updateTags = function(data) {
            self.tags(data);
        };

        // update citation
        this.updateCitations = function(data) {
            self.citations(data);
        };

        // unused tags to populate dropdown
        this.updateUnusedTags = function(data) {
            self.unusedTags(data);
        };

        function getHeaders() {
            var expr = new RegExp(/csrftoken=([\d\w]+)($|;)/g);
            var match = expr.exec(document.cookie) || [];

            return {'X-CSRFToken': match.length > 1 ? match[1] : ""};
        }

        // adds one tag
        function _addTag(tag) {
            self.tags.push(tag);
        };

        // add tag
        // check if exists first
        this.addTag = function() {
            // #96152072
            // enforce 5 tag limit
            if (self.tags().length >= 5) return;

            var tagToAdd = self.tagToAdd();

            if (!tagToAdd) return;

            _addTag(tagToAdd);

            self.tagToAdd("");
        };

        // add citation
        this.addCitation = function() {
            self.citations.push({
                link: self.citationLinkToAdd(),
                text: self.citationTextToAdd(),
                id: ""
            });
        };

        // remove tag
        this.removeTag = function(tag) {
            $.ajax({
                url: '/arguemeter/api/tags/' + tag.id + '/?article_id=' + self.id(),
                type: 'delete',
                headers: getHeaders(),
                complete: function() {
                    // remove it from viewmodel
                    var tags = self.tags();
                    for (var i = 0; i < tags.length; i++) {
                        if (tags[i].id === tag.id) {
                            tags.splice(i, 1);
                            self.tags(tags);
                        }
                    }
                }
            });
        };

        // remove citation
        this.removeCitation = function(citation) {
            $.ajax({
                url: '/arguemeter/api/citations/' + citation.id + '/',
                type: 'delete',
                headers: getHeaders(),
                complete: function() {
                    // remove it from viewmodel
                    var citations = self.citations();
                    for (var i = 0; i < citations.length; i++) {
                        if (citations[i].id === citation.id) {
                            citations.splice(i, 1);
                            self.citations(citations);
                            break;
                        }
                    }
                }
            });
        }
    };



    var tuvm = new ArticleUpdateViewModel();

    ko.applyBindings(tuvm, $("#articleUpdateModalForm")[0]);

    var articleElement = $("#article");
    if (articleElement.length) ko.applyBindings(tuvm, articleElement[0]);

    ko.applyBindings(tuvm, $(".sub-nav")[0]);


});