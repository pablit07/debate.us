// embedly preview setup
// makes request to embedly service and retrieves data
// http://embed.ly/docs/api/extract/endpoints/1/extract
$(function () {
    // check for embedly library existence
    if (!$.fn.preview) {
        console.warn('Embedly not found');
        return;
    }

    $('#id_full_url').preview({key: 'acbe471d9ecf4b9196d44c2ad6ded42c', render: function (response) {

        if (!response) return;

        // summary
        // removed per 8162228
//        if (response.description) $('#id_summary').val(response.description);

        // title
        if (response.title) $('#id_title').val(response.title);

        // removing this for now per 81602228
//        // render the preview image
//        if (response.images && response.images.length) {
//            var preview_image_data = response.images[0];
//            var img = $("<img src='" + preview_image_data.url + "' width='300' height='200'/>");
//            $('#shared_preview_container').html(img);
//        }

        // authors
        // removed per 81602228
//        if (response.authors && response.authors.length) {
//            $('#id_author').val($.map(response.authors, function (el) {
//                return el.name;
//            }).join('; '));
//        }

        // the provider url gives us roughly the publisher url without any extra manipulations
        if (response.provider_url)$('#id_publisher').val(response.provider_url);
    }});
});