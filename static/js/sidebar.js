/**
 * Created by paulkohlhoff on 12/14/14.
 */

// sets up sidebar open/close via button click event
$(function () {

    var sidebar = $('.sidebar-xs');

    // set up offscreen distance
    var offset = sidebar.outerWidth();

    sidebar.css(sidebar.hasClass('right') ? 'right' : 'left', '-'+offset+'px');

    // event to do sidebar expand/collapse from click on button with chevron icon
    $('body').on('click', '[data-toggle^=sidebar]', function (e) {
        var animate = {};
        var el = $(this);
        var icon = el.find('.glyphicon');
        var side = el.data('toggle').replace('sidebar', '');
        var opposite_side = side == "left" ? "right" : "left";
        var opposite_side_selector = "slide-" + opposite_side;
        var main = $('.main-content');

        // choose path based on buttons icon

        // swipe in
        if (icon.hasClass('glyphicon-chevron-' + opposite_side)) {
            icon.removeClass('glyphicon-chevron-' + opposite_side);
            icon.addClass('glyphicon-chevron-' + side);

            animate[side] = '' + offset + 'px';
            main.animate(animate, function () {
                // on done, reverse inline style and apply class instead

                animate[side] = '';

                if (main.hasClass(opposite_side_selector)) {
                    main.removeClass(opposite_side_selector);
                } else {
                    main.addClass(opposite_side_selector);
                }
            });

            animate[side] = '0px';
            sidebar.animate(animate, function () {
                // on done, reverse inline style and apply class instead
                animate[side] = '';

                sidebar.addClass(opposite_side_selector);
                sidebar.removeClass("slide-" + side);
            });

        // swipe out
        } else {
            icon.removeClass('glyphicon-chevron-' + side);
            icon.addClass('glyphicon-chevron-' + opposite_side);

            animate[side] = '0px';
            main.animate(animate, function () {
                // on done, reverse inline style and apply class instead

                animate[side] = '';

                if (main.hasClass(opposite_side_selector)) {
                    main.removeClass(opposite_side_selector);
                } else {
                    main.addClass(opposite_side_selector);
                }
            });

            animate[side] = '-' + offset + 'px';
            sidebar.animate(animate, {done: function () {
                // on done, reverse inline style and apply class instead

                animate[side] = '';

                sidebar.addClass("slide-" + side);
                sidebar.removeClass(opposite_side_selector);
            }});
        }
    });
});


$(function () {
    //Enable swiping...
    $("body").swipe({
        //Generic swipe handler for all directions
        swipe: function (event, direction) {
            if (direction != 'left' && direction != 'right') return;

            // disable when not in xs viewport width
            if (!$(event.srcElement).parents('body > .container').length || !$('.sidebar-xs:visible').length) return;

            var button = $("[data-toggle^=sidebar]");
            var lastSwiped = button.data('swiped');

            // click if hasnt been swiped or last swipe was opposite direction
            if (!lastSwiped || lastSwiped == (direction == 'right' ? 'left' : 'right')) {
                button.click();
                button.data('swiped', direction);
            }
        }
    });
});