//tinymce with bbcode
// this will attach tinymce to anything with the mce-editor class
        $(function () {
            $(".mce-editor").tinymce({
                theme: "modern",
                mode: "none",
                plugins: "bbcode paste placeholder",
                entity_encoding: "raw",
                add_unload_trigger: false,
                remove_linebreaks: false,
                inline_styles: false,
                convert_fonts_to_spans: false,
                menubar: false,
                toolbar1: "newdocument fullpage | bold italic | pastetext",
                setup: function(ed) {
                    // attach key events
                    ed.on('keyup', function (e) {
                        switch (e.keyCode) {
                            case 27:
                                // add escape to close behavior for inside of editor box
                                $('.modal.in').modal('hide');
                                break;
                        }
                    });
                }
            });
        });

function rate(rating, value){
    /* Modifies the rating value on the form to 'value' and fills in the stars
    *  up to the selected one. Removes all stars and rating if same star is clicked
    *  after being filled. */
    var form_value = $('#rating_' + rating + ', #rating_' + rating + '_xs');

    if(value == form_value.val()){
        form_value.val('-1');

        for(i=0; i<=5; i++) {
            var star = $('#rating_' + rating + '_' + i + ', #rating_' + rating + '_' + i + '_xs');

            star.removeClass('selected');
        }
    } else {
        form_value.val(value);
        add_stars(rating, value);
    }
}

function add_stars(rating, value){
    if (value > 0 && value <= 5) {
        for (var i = 1; i <= 5; i++) {
            var star = $('#rating_' + rating + '_' + i + ', #rating_' + rating + '_' + i + '_xs');

            if (i > value) {
                star.removeClass('selected');
            } else {
                star.addClass('selected');
            }
        }
    }
}

function slide_axis(axescat, value){
    if (value >= 0 && value <= 10){
        $('#axescat_' + axescat + '_' + value).prop("checked", true)
    }
}

// Agreeable decorator to provide classes with agree/disagree bidirectional behavior
var Agreeable = function(func) {
    var dis_agree_parameterized =
        function(userProp, countProp, userPropOpposite, countPropOpposite, dis_agree, outcome) {
            if (userProp()) {
                // unset
                dis_agree(null);
                countProp(countProp() - 1);
                return false;
            } else {
                // set
                if (userPropOpposite()) {
                    countPropOpposite(countPropOpposite() - 1)
                }
                dis_agree(outcome);
                countProp(countProp() + 1);
                return true;
            }
        };

    func.prototype.dis_agree_parameterized = dis_agree_parameterized;
};

var CommentViewModel = function(params) {
    this.comment_id = ko.observable(params.comment_id);
    this.user_dis_like = ko.observable(params.user_likes);
    this.likes = ko.observable(params.likes);
    this.dislikes = ko.observable(params.dislikes);
    this.numberOfReplies = ko.observable(
        params.numberOfReplies || 0
    );

    var url_add = '/arguemeter/comments/'+this.comment_id()+'/dis_agrees/add/';


    this.user_like = ko.computed(function() {
        return this.user_dis_like() == 'L';
    }, this);


    this.user_dislike = ko.computed(function() {
        return this.user_dis_like() == 'D';
    }, this);


    this.like = function() {
        var _set = this.dis_agree_parameterized(this.user_like, this.likes, this.user_dislike, this.dislikes, this.user_dis_like, 'L');
        $.get(url_add, {dis_like: _set ? 'True' : ''});
    };


    this.dislike = function() {
        var _set = this.dis_agree_parameterized(this.user_dislike, this.dislikes, this.user_like, this.likes, this.user_dis_like, 'D');
        $.get(url_add, {dis_like: _set ? 'False' : ''});
    }
};

Agreeable(CommentViewModel);

var DisAgreeViewModel = function(params) {
    this.article_agrees = ko.observable(params.article_agrees);
    this.article_disagrees = ko.observable(params.article_disagrees);
    this.user_article_dis_agree = ko.observable(params.user_article_dis_agree);


    this.user_article_disagree = ko.computed(function() {
        return this.user_article_dis_agree() === 'False';
    }, this);


    this.user_article_agree = ko.computed(function() {
        return this.user_article_dis_agree() === 'True';
    }, this);


    this.agreeArticle = function() {
        this.dis_agree_parameterized(this.user_article_agree, this.article_agrees, this.user_article_disagree, this.article_disagrees, this.user_article_dis_agree, 'True');
    };


    this.disagreeArticle = function() {
        this.dis_agree_parameterized(this.user_article_disagree, this.article_disagrees, this.user_article_agree, this.article_agrees, this.user_article_dis_agree, 'False');
    };
};

Agreeable(DisAgreeViewModel);

