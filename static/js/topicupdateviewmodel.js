//topic update knockout viewmodel
$(function () {

    var TopicUpdateViewModel = function () {
        var self = this;

        this.id = ko.observable(null);
        this.topic_name = ko.observable("");
        this.detail = ko.observable("");
        this.is_published = ko.observable("False");
        this.ratings = ko.observableArray([]);
        this.tags = ko.observableArray([]);
        this.citations = ko.observableArray([]);
        this.articles = ko.observableArray([]);

        //this.unusedTags = ko.observable([]);

        this.ratingToAdd = ko.observable("");
        this.tagToAdd = ko.observable("");
        this.citationLinkToAdd = ko.observable("");
        this.citationTextToAdd = ko.observable("");

        this.url = ko.computed(function() {
            return !!self.id() ? "/arguemeter/topics/" + self.id() + "/edit" : "/arguemeter/topics/add/quick/"
        });
        this.deleteUrl = ko.computed(function() { return "/arguemeter/topics/" + self.id() + "/delete/" });

        // do update
        this.id.subscribe(function (val) {

            // passing falsy for val clears data
            if (!val) {
                self.topic_name("");
                self.detail("");
                tinymce.get('id_update_detail').setContent("");
                self.is_published("false");
                self.ratings([]);
                self.tags([]);
                self.citations([]);
                self.articles([]);
                // adds the ratings marked as default to the view
                $.get("api/ratings/?is_default=True", self.updateRatings);
            } else {
                self.updating = true;
                $.get("api/topics/" + val + "/", self.updateTopic);
                $.get("api/ratings/?topic__id=" + val, self.updateRatings);
                $.get("api/tags/?topic__id=" + val, self.updateTags);
                $.get("api/citations/?topic__id=" + val, self.updateCitations);
                //$.get("api/tags/?-topic__id=" + val, self.updateUnusedTags);
                $.get("api/article/?topic__id=" + val, self.updateArticles);
            }
        });

        // bind click events to update the id
        this.updateId = function (data, e) {
            var el = $(e.currentTarget),
                val = el.data('value');

            self.id(val);
            self.ratingToAdd("");
            self.tagToAdd("");
            self.citationLinkToAdd("");
            self.citationTextToAdd("");

            // check if we caused an update (if id changed)
            if (self.updating) {
                self.updating = false;
            } else {
                var editor = tinymce.get('id_update_detail');
                editor.focus();
            }


        };

        // updates topic model from api call
        this.updateTopic = function(data) {
            self.topic_name(data.topic_name);
            self.is_published(data.is_published);
            // update the content
            // http://stackoverflow.com/questions/8505776/how-to-dynamically-replace-content-in-tinymce
            var editor = tinymce.get('id_update_detail');
            var content = data.detail;
            editor.setContent(content);
            editor.focus();
        };

        // updates ratings
        this.updateRatings = function(data) {
            self.ratings(data);
            self._dragAndDrop();
        };

        // update tags
        this.updateTags = function(data) {
            self.tags(data);
        };

        // update citation
        this.updateCitations = function(data) {
            self.citations(data);
        };

        // unused tags to populate dropdown
        this.updateUnusedTags = function(data) {
            self.unusedTags(data);
        };

        // update articles
        this.updateArticles = function(data) {
            self.articles(data);
        };

        function getHeaders() {
            var expr = new RegExp(/csrftoken=([\d\w]+)($|;)/g);
            var match = expr.exec(document.cookie) || [];

            return {'X-CSRFToken': match.length > 1 ? match[1] : ""};
        }

        // adds a single rating
        function _addRating(rating) {
            self.ratings.push(rating);
            self._dragAndDrop();
        }

        self._dragAndDrop = function() {
            $(".data.rating").draggable({
                revert: true,
                revertDuration: 0,
                start: function(e, ui) {
                    var draggable = $(e.target);
                    draggable.css('z-index', 1000);
                },
                stop: function(e, ui) {
                    var draggable = $(e.target);
                    draggable.css('z-index', '');
                }
            });
            $(".data.rating").droppable({
                drop: function( e, ui ) {

                    var ratings = self.ratings(),
                        target = $(e.target),
                        target_index = Number(target.attr('data-index')),
                        draggable = ui.draggable,
                        draggable_index = Number(draggable.attr('data-index'));

                    var tmp = ratings[target_index];
                    ratings[target_index] = ratings[draggable_index];
                    ratings[draggable_index] = tmp;
                    self.ratings(ratings);
                }
            });
        };

        // add rating with database add
        // will reuse default ratings
        this.addRating = function() {

            // check if already exists amongst default
            $.get('api/ratings/?name=' + self.ratingToAdd() + '&is_current_user=True', function(match) {
                if (match.length) {
                    _addRating(match[0]);
                } else {
                    $.ajax({
                        url: 'api/ratings/',
                        type: 'post',
                        headers: getHeaders(),
                        data: {
                            name: self.ratingToAdd(),
                            weight: 3
                        },
                        success: _addRating
                    });
                }
                self.ratingToAdd("");
            });


        }

        // add tag
        this.addTag = function() {
            $.ajax({
                url: 'api/tags/',
                type: 'post',
                headers: getHeaders(),
                data: {
                    name: self.tagToAdd()
                },
                success: function(tag) {
                    self.tags.push(tag);
                }
            });
            // remove it from list
            //var unusedTags = self.unusedTags();
            //for (var i = 0; i < unusedTags.length; i++) {
            //    if (unusedTags[i].id === tag.id) {
            //        unusedTags.splice(i, 1);
            //        self.unusedTags(unusedTags);
            //        break;
            //    }
            //}
            self.tagToAdd("");
        };

        // add citation
        this.addCitation = function() {
            self.citations.push({
                link: self.citationLinkToAdd(),
                text: self.citationTextToAdd(),
                id: ""
            });
        };

        // remove rating
        this.removeRating = function(rating) {
            $.ajax({
                url: 'api/ratings/' + rating.id + '/?topic_id=' + self.id(),
                type: 'delete',
                headers: getHeaders(),
                complete: function() {
                    // remove it from viewmodel
                    var ratings = self.ratings();
                    for (var i = 0; i < ratings.length; i++) {
                        if (ratings[i].id === rating.id) {
                            ratings.splice(i, 1);
                            self.ratings(ratings);
                            break;
                        }
                    }
                }
            });
        };

        // remove tag
        this.removeTag = function(tag) {
            $.ajax({
                url: 'api/tags/' + tag.id + '/?topic_id=' + self.id(),
                type: 'delete',
                headers: getHeaders(),
                complete: function() {
                    // remove it from viewmodel
                    var tags = self.tags();
                    for (var i = 0; i < tags.length; i++) {
                        if (tags[i].id === tag.id) {
                            tags.splice(i, 1);
                            self.tags(tags);
                        }
                    }
                }
            });
        }

        // remove citation
        this.removeCitation = function(citation) {
            $.ajax({
                url: 'api/citations/' + citation.id + '/',
                type: 'delete',
                headers: getHeaders(),
                complete: function() {
                    // remove it from viewmodel
                    var citations = self.citations();
                    for (var i = 0; i < citations.length; i++) {
                        if (citations[i].id === citation.id) {
                            citations.splice(i, 1);
                            self.citations(citations);
                            break;
                        }
                    }
                }
            });
        }
    };

    var tuvm = new TopicUpdateViewModel();

    ko.applyBindings(tuvm, $("#topicUpdateModalForm")[0]);
    ko.applyBindings(tuvm, $("#topics")[0]);
    ko.applyBindings(tuvm, $(".title-bar-inner")[0]);

});
