this is a readme file

# Argument List

Display topic question, moderator, and date created
Expand/hide "more information" module containing the 
Display argument title, author, date, quality score, and agree count for each argument in list
Display arguments sorted by quality score (default)
Display sort and filter menu and search bar
Sort argument by score or date
Filter arguments by reasons
Select argument to read

# View Argument

Display argument title, author, date, quality score, and agree count in heading
Display argument text, scroll through argument text
Display rating module (with agree/disagree selection and quality selections) to right of argument text, remains static when scrolling through argument text
Switch from rating module to comment module using tabs at top of module
Display comment author, date/time, text, and up/down votes for each comment
Display comment tiles in order of most recent to oldest (default)
Show new comment tile appearing
Draft comment and submit
Up/down vote comment
Sort comment tiles in order of most upvotes

# Draft Argument

Display topic question in header
Display form for selecting author (default username or pseudonym/anonymous are allowed) and entering argument title
Display form for entering and editing text, including basic formatting menu
Display form for selecting and weighting reasons
Display form for entering sources
Add link (read if link works)
Enter full citation
Submit argument and return to arguments list

# Manage topics

In page header, display name and organization of moderator 
Display list of topics created by moderator (default: in order of date created)
Display question, network, and date created for each topic
Select edit topic or create new topic
Display form for entering/editing topic information:
Topic Question (50 words)
More information (500 words)
Display module for creating rubric for topic
Display default attributes: Clear, Compelling, Credible
Select quality attributes from drop down menu
Delete selected quality attribute (via roll-over (x))
Enter new quality attribute (10 words)
Set weight for each attribute by percent (default even weight)
Add sources for topic
Add link (read if link works)
Enter full citation
Select network from drop-down menu (e.g., class, department, public) (only users with access to network can see topic) (moderator can only access networksthat 
Save information
