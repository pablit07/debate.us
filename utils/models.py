# Python imports

# Core Django imports
from django.db import models
from django.contrib.auth.models import User


class UserData(models.Model):
    user = models.ForeignKey(User)
    # organization = models.ForeignKey(Organization, blank=True, null=True)
    # team = models.ForeignKey(Team, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_last_edited = models.DateTimeField(blank=True, null=True)
    deleted = models.BooleanField(default=False)
    date_deleted = models.DateTimeField(blank=True, null=True)
    # removed per 1107370
    # VALUES = (
    #     (1, "Username"),
    #     (2, "Full Name"),
    #     (3, "Pseudonym"),
    #     (4, "Anonymous"),
    # )
    # author = models.PositiveSmallIntegerField(max_length=1, choices=VALUES, default=1)

    class Meta:
        abstract = True

    @property
    def display_author(self):
        full_name = self.user.get_full_name()
        if not full_name:
            return self.user.username
        else:
            return full_name
        # removed per 1107370
        # if self.author == 1:
        #     return self.user.username
        # elif self.author == 2:
        #     return self.user.get_full_name()
        # elif self.author == 3:
        #     return self.user.userprofile.pseudonym
        # elif self.author == 4:
        #     return "Anonymous"