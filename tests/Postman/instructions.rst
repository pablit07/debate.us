OAuth 2.0 Instructions
=====================

This document is designed to guide a new developer/tester through setting up OAuth 2.0 for client requests and testing.

Prerequisites
=====================

Postman
---------------------

Use the Chrome addon Postman

.. _Postman Home Page: http://www.getpostman.com

Package requirements
---------------------

From the directory where you cloned the git repo run:

	**$** *pip install -r requirements.txt*

Setup Steps
=====================

Use these steps to setup a new user with OAuth credentials:

	1. In the Django Admin interface, add a new user named professor_test1 
	2. When the user has been created, go back to the main admin interface. 
	3. Under Oauth2, click the +Add button next to "Clients". Fill in the fields as needed using:
		http://localhost:8000/admin/oauth2/client/3/

	: for all URLs. 
	4. Set client type to be Public. 
	5. Save the client.
	6. From the command prompt, run:
		**$** *curl -d "client_id=d63f53a7a6cceba04db5&client_secret=afe899288b9ac4127d57f2f12ac5a49d839364dc&grant_type=password&username=ian&password=straightflexin&scope=write" http://localhost:8000/oauth2/access_token*
	  : replacing client_id, client_secret, username, and password with the relevant bits for your user and associated client info.
	7. Hit [enter] and you should receive an access_token

If you have any issues, further documentation can be found at:

.. _Building a True OAuth 20 API with Django and Tasty Pie: http://ianalexandr.com/blog/building-a-true-oauth-20-api-with-django-and-tasty-pie.html

Testing
=====================

Use these steps to verify that the OAuth token is functioning:
	
	1. Open Chrome
	2. Open Postman
	3. Go to collections
	4. Import collection (button that looks like an inbox)
	5. Select the DebateUs.json file from <git repo>/tests_api
	6. Open the "create topic - professor" request.
	7. Replace the '3' in the user URI with the ID number of your professor_test1 user.
	8. In the upper-right corner, select Headers (3)
	9. Replace the value after OAuth with your access token
	10. Click "Send"
	11. You should receive a "201 Created" response
	12. Perform the same OAuth access token replacement for all "professor" requests and for "get topic json array"
	13. Run the "create article - professor" request
	14. You should receive a "201 Created" response
	15. Run the "get topic json array" request
	16. You should receive a JSON array reflecting the topic and associated article.