from django import template
from django.conf import settings
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
def bbcode(value):
    """
    Generates (X)HTML from string with BBCode "markup".
    By using the postmark lib from:
    @see: http://code.google.com/p/postmarkup/

    """
    try:
        from postmarkup import render_bbcode
    except ImportError:
        if settings.DEBUG:
            raise template.TemplateSyntaxError, "Error in {% bbcode %} filter: The Python postmarkup library isn't installed."
        return force_unicode(value)
    else:
        output = render_bbcode(value, render_unknown_tags=True)
        output = output.replace('[br]', '<br />')
        return mark_safe(output)
bbcode.is_save = True


@register.filter
def redfill(percent):

    # this assumes we're using just for the rating remainder
    percent = float((percent % 1)) * 100

    r_min = 204.0
    color_max = 255.0

    r_rate = -1.0 * (color_max - r_min) / 100
    gb_rate = -1.0 * (color_max / 100)

    r = int(round(color_max + (r_rate * percent), 0))
    g = int(round(color_max + (gb_rate * percent), 0))
    b = g

    return "color:rgb({r}, {g}, {b})".format(r=r, g=g, b=b)