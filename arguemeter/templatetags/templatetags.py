from django import template
from arguemeter.forms import CommentForm, CitationForm, TopicForm
from core.models import Citation, TopicFavorite
from messages.models import Inbox, Message

register = template.Library()


@register.inclusion_tag('core/comment_form.html', )
def add_comment(article_id, parent=None):
    return {'parent': parent, 'article_id': article_id, 'comment_form': CommentForm()}


@register.inclusion_tag('core/citation_form.html')
def add_citation(form=CitationForm()):
    return {'form': form}


@register.inclusion_tag('core/citation_list.html')
def view_citation_list(cols=7, topic_id=None, article_id=None, class_str=""):
    if not (topic_id or article_id):
        raise ValueError("Must pass either an article id or a topic id to citation list template.")

    if topic_id:
        citations = Citation.objects.filter(topic_id=topic_id)
    elif article_id:
        citations = Citation.objects.filter(article_id=article_id)

    # need to add ellipses after 100 chars
    for citation in citations:
        if len(citation.text) > 100:
            citation.text = citation.text[:100]
            citation.is_truncated = True

    return {'citations': citations, 'cols': cols, 'class_str': class_str}


@register.inclusion_tag('core/sub_nav.html')
def sub_nav(topic, form, shared_form_pg1, shared_form_pg2):
    return {'topic': topic,
            'form': form,
            'shared_form_pg1': shared_form_pg1,
            'shared_form_pg2': shared_form_pg2}

@register.inclusion_tag('core/topic_description.html')
def topic_description(topic):
    return {'topic': topic}


@register.inclusion_tag("core/article_header_topic_title.html")
def article_header_topic_title(topic, request):
    favorite = TopicFavorite.objects.filter(topic=topic, user=request.user, deleted=False)
    return {'topic': topic,
            'is_favorite': favorite.exists()}


@register.inclusion_tag("messages/username_dropdown_title.html")
def username_dropdown_title(user):
    inboxes = Inbox.objects.filter(deleted=False)
    messages = Message.objects.filter(inbox__in=inboxes, deleted=False).exclude(read_by__id=user.id)
    message_count = messages.count()
    return {'user': user,
            'message_count': message_count}