from rest_framework import serializers
from core.models import Topic, Rating, AxesCategorization, Citation, Article, ReasonTag, LinkSummary

__author__ = 'paulkohlhoff'


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic


class LinkSummarySerializer(serializers.ModelSerializer):
    publisher = serializers.ReadOnlyField()

    class Meta:
        model = LinkSummary


class ArticleSerializer(serializers.ModelSerializer):
    shared_link = LinkSummarySerializer(read_only=True)

    class Meta:
        model = Article


class RatingSerializer(serializers.ModelSerializer):
    topic = TopicSerializer(many=True, read_only=True)

    class Meta:
        model = Rating


class AxesCategorizationSerializer(serializers.ModelSerializer):
    topic = TopicSerializer(many=True, read_only=True)

    class Meta:
        model = AxesCategorization


class CitationSerializer(serializers.ModelSerializer):
    topic = TopicSerializer(read_only=True)

    class Meta:
        model = Citation


class ReasonTagSerializer(serializers.ModelSerializer):
    topic = TopicSerializer(many=True, read_only=True)

    class Meta:
        model = ReasonTag