from crispy_forms.helper import FormHelper
from django.forms import HiddenInput, Field

__author__ = 'archen'

# Core Python imports
from datetime import datetime

# Core Django imports
from django import forms
from django.utils.timezone import utc

# Third party imports

# App-specific imports
from core.models import Article, Citation, Comment, Topic, LinkSummary, Link


# override to get placeholder text attribute
# i may be doing this wrong...
# http://django-crispy-forms.readthedocs.org/en/1.1.1/tags.html
class TextWithPlaceholder(forms.TextInput):
    def __init__(self, placeholder, attrs={}):
        super(TextWithPlaceholder, self).__init__(attrs=attrs)
        self.placeholder = placeholder

    def render(self, name, value, attrs={}):
        attrs['placeholder'] = self.placeholder
        return super(TextWithPlaceholder, self).render(name, value, attrs=attrs)


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ('article_name', 'content')

    article_name = Field(
        widget=TextWithPlaceholder("Title", attrs={'data-bind': 'value: article_name'}),
        label=""
    )

    content = Field(
        widget=TextWithPlaceholder("Write your argument here", attrs={'class': 'mce-editor', 'data-bind':
            'value: content'}),
        label=""
    )


def clean_date_last_edited(self):
    return datetime.utcnow().replace(tzinfo=utc)


class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = ('topic_name', 'detail', 'is_published')

    topic_name = Field(
        widget=TextWithPlaceholder("Enter topic title", attrs={'data-bind': 'value: topic_name'}),
        label=""
    )

    detail = Field(
        widget=TextWithPlaceholder("Enter topic description", attrs={'class': 'mce-editor', 'data-bind':
            'value: detail'}),
        label=""
    )

    is_published = Field(
        widget=HiddenInput(attrs={'data-bind': 'value: is_published'}),
        required=False
    )

    def clean_date_last_edited(self):
        return datetime.utcnow().replace(tzinfo=utc)

    def clean_is_published(self):
        return self.cleaned_data['is_published'].capitalize()


class CitationForm(forms.ModelForm):
    class Meta:
        model = Citation
        fields = ('text', 'link')


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('comment_text',)


class LinkForm(forms.Form):
    full_url = Field(
        widget=TextWithPlaceholder("Paste or enter a link to an article or YouTube video",
                                   attrs={'data-bind': 'value: shared_link_url'}),
        label=""

    )


class LinkSummaryForm(forms.Form):
    title = Field(
        widget=TextWithPlaceholder("Title", attrs={'data-bind': 'value: shared_link_title'}),
        label=""
    )

    author = Field(
        widget=TextWithPlaceholder("Author", attrs={'data-bind': 'value: shared_link_author'}),
        label=""
    )

    summary = Field(
        widget=TextWithPlaceholder("Summary", attrs={'class': 'mce-editor'}),
        label=""
    )

    publisher = Field(
        widget=TextWithPlaceholder("Publisher", attrs={'data-bind': 'value: shared_link_publisher'}),
        label=""
    )

    def __init__(self):
        super(LinkSummaryForm, self).__init__()
        # this is required because this form is intended to be grouped along with the LinkForm
        self.helper = FormHelper()
        self.helper.form_tag = False
