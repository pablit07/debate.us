# Core Python imports
import ast
from datetime import datetime
from exceptions import ValueError
from itertools import chain

# Core Django imports
from django.contrib.auth import get_user_model
from crispy_forms.helper import FormHelper
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count, Sum
from django.db.utils import IntegrityError
from django.http import HttpResponse, HttpResponseServerError
from django.shortcuts import redirect, render, get_object_or_404
from django.utils.timezone import utc
from django.views.generic.edit import UpdateView

# Third Party imports
from braces.views import LoginRequiredMixin
# from guardian.shortcuts import assign_perm, get_objects_for_user

# Application imports
from rest_framework import viewsets, generics
from rest_framework.response import Response
from arguemeter.serializers import TopicSerializer, RatingSerializer, AxesCategorizationSerializer, CitationSerializer, \
    ArticleSerializer, ReasonTagSerializer
from .forms import ArticleForm, CitationForm, CommentForm, TopicForm, LinkForm, LinkSummaryForm
# from community.authorization import permission_required_or_403
# from community.models import Community, CommunityTag
from core.models import Article, AxesCategorization, Citation, Comment, DisAgree, DisAgreeAxis, DisLike, Rating, \
    RatingCriteria, Topic, Link, LinkSummary, ReasonTag, DisAgreeTag, TopicFavorite
from core import rater
from core.authorization import ArticleOwnerMixin, TopicOwnerMixin, UserOwnerMixin, is_article_owner, \
    is_not_article_owner, is_topic_owner


class TopicViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows topics to be viewed or edited.
    """
    queryset = Topic.objects.filter(deleted=False)
    serializer_class = TopicSerializer

    def destroy(self, request, *args, **kwargs):
        topic = self.get_object()
        topic.deleted = True
        topic.save()


class ArticleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows articles to be viewed or edited.
    """
    queryset = Article.objects.filter(deleted=False)
    serializer_class = ArticleSerializer
    filter_fields = ('topic__id',)

    def destroy(self, request, *args, **kwargs):
        article = self.get_object()
        article.deleted = True
        article.save()


class RatingViewSet(viewsets.ModelViewSet):
    serializer_class = RatingSerializer
    queryset = Rating.objects.filter(deleted=False).order_by('order')
    filter_fields = ('topic__id', 'name', 'is_default')

    def create(self, request, *args, **kwargs):
        try:
            post = request.POST
            rating = Rating(name=post['name'], description=post['name'], weight=post['weight'],
                            date_created=datetime.utcnow().replace(tzinfo=utc),
                            date_last_edited=datetime.utcnow().replace(tzinfo=utc), owner=request.user)
            rating.save()
            return Response(data=self.serializer_class(rating).data)
        except:
            return HttpResponseServerError()

    def destroy(self, request, *args, **kwargs):
        params = request.QUERY_PARAMS
        if 'topic_id' in params:
            rating = Rating.objects.get(id=kwargs.get('pk'))
            topic = Topic.objects.get(id=params['topic_id'])
            rating.topic.remove(topic)
            return HttpResponse()
        else:
            return super(RatingViewSet, self).destroy(request, args, kwargs)

    def list(self, request, *args, **kwargs):
        params = request.QUERY_PARAMS
        if params.get('is_current_user'):
            queryset = Rating.objects.filter(name=params.get('name'), owner=request.user)
            serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data)
        else:
            return super(RatingViewSet, self).list(request, args, kwargs)


class AxesCategorizationViewSet(viewsets.ModelViewSet):
    serializer_class = AxesCategorizationSerializer
    queryset = AxesCategorization.objects.filter(deleted=False)
    filter_fields = ('topic__id', 'article__id', 'name',)

    def get_queryset(self):
        queryset = super(AxesCategorizationViewSet, self).get_queryset()

        topic_id = self.request.query_params.get('-topic__id', None)
        article_id = self.request.query_params.get('-article__id', None)

        if topic_id:
            queryset = queryset.exclude(topic__id=topic_id)

        if article_id:
            queryset = queryset.exclude(article__id=article_id)

        return queryset

    def destroy(self, request, *args, **kwargs):
        params = request.QUERY_PARAMS
        if 'topic_id' in params:
            axes_cat = AxesCategorization.objects.get(id=kwargs.get('pk'))
            topic = Topic.objects.get(id=params['topic_id'])
            axes_cat.topic.remove(topic)
            return HttpResponse()
        else:
            return super(AxesCategorizationViewSet, self).destroy(request, args, kwargs)


class ReasonTagViewSet(viewsets.ModelViewSet):
    serializer_class = ReasonTagSerializer
    queryset = ReasonTag.objects.filter(deleted=False)
    filter_fields = ('topic__id', 'article__id', 'name',)

    def get_queryset(self):
        queryset = super(ReasonTagViewSet, self).get_queryset()

        # add exclusion behavior
        topic_id_exclude = self.request.query_params.get('-topic__id', None)
        article_id_exclude = self.request.query_params.get('-article__id', None)

        if topic_id_exclude:
            queryset = queryset.exclude(topic__id=topic_id_exclude)

        if article_id_exclude:
            queryset = queryset.exclude(article__id=article_id_exclude)

        return queryset

    def create(self, request, *args, **kwargs):
        post = request.POST
        tag = ReasonTag(name=post['name'], user=request.user)
        tag.save()
        return Response(data=self.serializer_class(tag).data)

    def destroy(self, request, *args, **kwargs):
        params = request.QUERY_PARAMS
        if 'topic_id' in params:
            tag = self.get_object()
            topic = Topic.objects.get(id=params['topic_id'])
            tag.topic.remove(topic)
            return HttpResponse()
        elif 'article_id' in params:
            tag = self.get_object()
            article = Article.objects.get(id=params['article_id'])
            tag.article.remove(article)
            tag.topic.remove(article.topic)
            return HttpResponse()
        else:
            return super(ReasonTagViewSet, self).destroy(request, args, kwargs)


class CitationViewSet(viewsets.ModelViewSet):
    serializer_class = CitationSerializer
    queryset = Citation.objects.filter(deleted=False)
    filter_fields = ('topic__id', 'article__id',)


class ArticleUpdate(LoginRequiredMixin, ArticleOwnerMixin, UpdateView):
    model = Article
    form_class = ArticleForm

    login_url = "/accounts/login/"

    def post(self, request, *args, **kwargs):
        result = super(ArticleUpdate, self).post(request, args, kwargs)

        article = self.object

        # new article needs user/author set
        if not article.user:
            article.user = request.user
            article.save()

        # save axes
        # for axes_cat_id in request.POST.getlist('axes_use[]'):
        #     axes_cat = AxesCategorization.objects.get(pk=axes_cat_id)
        #     axes_cat.article.add(article)
        #     axes_cat.used_by.add(request.user)
        #     axes_cat.save()
        # save tags
        for tag_id in request.POST.getlist('tag_use[]'):
            tag = ReasonTag.objects.get(pk=tag_id)
            tag.article.add(article)
            tag.topic.add(article.topic)
            tag.used_by.add(request.user)
            tag.save()
        # save citations
        for citation_text, citation_link, citation_id in zip(request.POST.getlist('citation_text[]'),
                                                             request.POST.getlist('citation_link[]'),
                                                             request.POST.getlist('citation_id[]')):
            if not citation_id:
                citation = Citation(user_id=request.user.id, article=article, text=citation_text,
                                    link=citation_link, date_created=datetime.utcnow().replace(tzinfo=utc),
                                    date_last_edited=datetime.utcnow().replace(tzinfo=utc))
                citation.save()

        # check for link (shared article) update
        if request.POST.get('shared_link_id'):
            link_summary = LinkSummary.objects.get(id=request.POST.get('shared_link_id'))
            link_summary.author = request.POST.get('author')
            link_summary.title = request.POST.get('title')
            link_summary.summary = request.POST.get('summary')

            if link_summary.link.full_url != request.POST.get('full_url'):
                existing = Link.objects.filter(full_url=request.POST.get('full_url'))
                if existing.exists():
                    link_summary.link = existing.get()
                else:
                    link = Link(full_url=request.POST.get('full_url'))
                    link.save()
                    link_summary.link = link
            link_summary.save()

            article.content = request.POST.get('summary')
            article.save()

        return result


class TopicUpdate(LoginRequiredMixin, TopicOwnerMixin, UpdateView):
    model = Topic
    form_class = TopicForm

    login_url = "/accounts/login/"

    def post(self, request, *args, **kwargs):

        result = super(TopicUpdate, self).post(request, args, kwargs)

        if result.status_code == 200 or result.status_code == 302:

            topic = self.object

            # new topic needs user/author set
            if not topic.user:
                topic.user = request.user
                topic.save()

            # save ratings
            order = 0

            for rating_id in request.POST.getlist('rating_use[]'):
                order += 1
                rating = Rating.objects.get(pk=rating_id)
                rating.topic.add(topic)
                rating.used_by.add(request.user)
                rating.order = order
                rating.save()
            # save axes
            # for axes_cat_id in request.POST.getlist('axes_use[]'):
            #     axes_cat = AxesCategorization.objects.get(pk=axes_cat_id)
            #     axes_cat.topic.add(topic)
            #     axes_cat.used_by.add(request.user)
            #     axes_cat.save()
            # save tags
            for tag_id in request.POST.getlist('tag_use[]'):
                tag = ReasonTag.objects.get(pk=tag_id)
                tag.topic.add(topic)
                tag.used_by.add(request.user)
                tag.save()
            # save citations
            for citation_text, citation_link, citation_id in zip(request.POST.getlist('citation_text[]'),
                                                                 request.POST.getlist('citation_link[]'),
                                                                 request.POST.getlist('citation_id[]')):
                if not citation_id:
                    citation = Citation(user_id=request.user.id, topic=topic, text=citation_text,
                                        link=citation_link, date_created=datetime.utcnow().replace(tzinfo=utc),
                                        date_last_edited=datetime.utcnow().replace(tzinfo=utc))
                    citation.save()

            return redirect('arguemeter:list-articles', topic.id)
        # error condition
        return result


class ProfileUpdate(LoginRequiredMixin, UserOwnerMixin, UpdateView):
    model = get_user_model()
    fields = ('first_name', 'last_name', 'email',)
    success_url = reverse_lazy('arguemeter:view-profile')


@login_required
def index(request):
    if request.POST.get('orderby'):
        request.session['orderbytopic'] = request.POST.get('orderby')

    context = construct_topic_list_context(request)
    return render(request, 'core/topic_list.html', context)


@login_required
def profile_view(request):
    user = get_object_or_404(get_user_model(), pk=request.user.id)

    context = {
        'profile': user
    }

    return render(request, 'arguemeter/profile_view.html', context)


# @permission_required_or_403('view_article', (Article, 'id', 'pk'))
def article_detail_view(request, pk):
    context = construct_article_detail_context(request, pk)

    form = ArticleForm()
    shared_form_pg1 = LinkForm()
    shared_form_pg2 = LinkSummaryForm()

    context['form'] = form
    context['shared_form_pg1'] = shared_form_pg1
    context['shared_form_pg2'] = shared_form_pg2

    return render(request, 'core/article_detail.html', context)


# @permission_required_or_403('view_topic', (Topic, 'id', 'pk'))
def topic_articles_list_view(request, pk, context=None):
    topic = get_object_or_404(Topic, id=pk, deleted=False)
    # article_list = get_objects_for_user(request.user, 'view_article', Article)\
    #     .filter(topic=topic, deleted=False).order_by('-rating_score')
    checked_tags = None

    if request.GET and request.GET.get('orderby'):
        orderby = request.GET.get('orderby')
    else:
        orderby = '-rating_score'

    if request.GET and request.GET.get('search'):
        article_list = Article.objects.filter(topic=topic, deleted=False, article_name=request.GET.get('search'))
    else:
        article_list = Article.objects.filter(topic=topic, deleted=False)

    # adds reason tag filter
    # 96249914
    if request.GET:
        checked_tags = request.GET.getlist('tag_id[]')
        checked_tags = map(lambda t: int(t), checked_tags)

    # if none, then no filtering on tags
    if checked_tags:
        article_list = article_list.filter(reasontag__id__in=checked_tags)

    article_list = article_list.annotate(num_comments=Count('comment')).order_by(orderby)

    n_shared = Article.objects.filter(topic=topic, deleted=False, topic__deleted=False).exclude(shared_link=None).count()
    n_original = Article.objects.filter(topic=topic, deleted=False, topic__deleted=False, shared_link=None).count()

    form = ArticleForm()
    shared_form_pg1 = LinkForm()
    shared_form_pg2 = LinkSummaryForm()

    tags = ReasonTag.objects.filter(topic=topic).annotate(num_agrees=Sum('disagreetag__score')).order_by('num_agrees')

    update_form = ArticleForm(auto_id="id_update_%s")
    update_form.helper = FormHelper()
    update_form.helper.form_tag = False

    if not context:
        context = {
            'topic': topic,
            'article_list': article_list,
            'form': form,
            'shared_form_pg1': shared_form_pg1,
            'shared_form_pg2': shared_form_pg2,
            'n_shared': n_shared,
            'n_original': n_original,
            'tags': tags,
            'checked_tags': checked_tags,
            'update_form': update_form
        }
    else:
        context.update({
            'topic': topic,
            'article_list': article_list,
            'form': form,
            'shared_form_pg1': shared_form_pg1,
            'shared_form_pg2': shared_form_pg2,
            'n_shared': n_shared,
            'n_original': n_original,
            'tags': tags,
            'checked_tags': checked_tags,
            'update_form': update_form
        })

    return render(request, 'core/article_list.html', context)


@login_required
@is_topic_owner
def delete_topic(request, pk):
    try:
        topic = get_object_or_404(Topic, pk=pk)
        topic.deleted = True
        topic.save()

    except IntegrityError:
        context = construct_topic_list_context(request)

        context.update({'error_message': 'Error deleting topic.'}, )

        return render(request, 'core/topic_list.html', context)

    return redirect("arguemeter:index")


@login_required
# @permission_required_or_403('add_article', (Topic, 'id', 'pk'))
def article_add_view(request, pk, context=None):
    topic = get_object_or_404(Topic, pk=pk, deleted=False)
    # topic_communities = topic.communities.all()
    # user_communities = get_objects_for_user(request.user, 'contribute', Community).filter(deleted=False)
    # article_communities = []

    # for community in topic_communities:
    #     if community in user_communities:
    #         article_communities.append(community)

    if context:
        topic_articles_list_view(request, topic.id, context)

    # check for link (shared article) from form
    elif request.POST.get('full_url'):

        try:
            existing = Link.objects.filter(full_url=request.POST.get('full_url'))
            if existing.exists():
                link = existing.get()
            else:
                link = Link(full_url=request.POST.get('full_url'))
                link.save()
            link_summary = LinkSummary(author=request.POST.get('author'), link=link, user=request.user,
                                       summary=request.POST.get('summary'), title=request.POST.get('title'))
            link_summary.save()

            article = Article(user_id=request.user.id, topic_id=topic.id, article_name=request.POST.get('title'),
                              date_created=datetime.utcnow().replace(tzinfo=utc), content=request.POST.get('summary'),
                              date_last_edited=datetime.utcnow().replace(tzinfo=utc), rating_score=0,
                              rating_algorithm=getattr(rater.get_rater(), 'algorithm'),
                              shared_link=link_summary)
            article.save()

            topic.article_date_last_edited = article.date_last_edited
            topic.save()

            # save tags
            for tag_id in request.POST.getlist('tag_use[]'):
                tag = ReasonTag.objects.get(pk=tag_id)
                tag.article.add(article)
                tag.topic.add(article.topic)
                tag.used_by.add(request.user)
                tag.save()
            # save citations
            for citation_text, citation_link, citation_id in zip(request.POST.getlist('citation_text[]'),
                                                                 request.POST.getlist('citation_link[]'),
                                                                 request.POST.getlist('citation_id[]')):
                if not citation_id:
                    citation = Citation(user_id=request.user.id, article=article, text=citation_text,
                                        link=citation_link, date_created=datetime.utcnow().replace(tzinfo=utc),
                                        date_last_edited=datetime.utcnow().replace(tzinfo=utc))
                    citation.save()

            return redirect('arguemeter:list-articles', topic.id)

        except IntegrityError:
            article_list = Article.objects.filter(topic=topic, deleted=False).order_by('-rating_score')
            form = ArticleForm
            shared_form_pg1 = LinkForm()
            shared_form_pg2 = LinkSummaryForm()
            update_form = ArticleForm(auto_id="id_update_%s")
            update_form.helper = FormHelper()
            update_form.helper.form_tag = False

            context = {
                'topic': topic,
                'article_list': article_list,
                'form': form,
                'error_message': 'Error sharing article. Has this url already been used under this topic?',
                'shared_form_pg1': shared_form_pg1,
                'shared_form_pg2': shared_form_pg2,
                'update_form': update_form
            }

            return render(request, 'core/article_list.html', context)

    else:
        try:
            article = Article(user_id=request.user.id, topic_id=topic.id, article_name=request.POST.get('article_name'),
                              content=request.POST.get('content'),
                              date_created=datetime.utcnow().replace(tzinfo=utc),
                              date_last_edited=datetime.utcnow().replace(tzinfo=utc), rating_score=0,
                              rating_algorithm=getattr(rater.get_rater(), 'algorithm'))
            article.save()

            topic.article_date_last_edited = article.date_last_edited
            topic.save()

            # for community in article_communities:
            #     assign_perm('comment', community.group, article)
            #     assign_perm('rate_article', community.group, article)
            #     assign_perm('view_article', community.group, article)
            #     tag = CommunityTag(community=community, content_type=topic.content_type, object_id=article.id)
            #     tag.save()

            for tag_id in request.POST.getlist('tag_use[]'):
                tag = ReasonTag.objects.get(pk=tag_id)
                tag.article.add(article)
                tag.topic.add(article.topic)
                tag.used_by.add(request.user)
                tag.save()
            # save citations
            for citation_text, citation_link, citation_id in zip(request.POST.getlist('citation_text[]'),
                                                                 request.POST.getlist('citation_link[]'),
                                                                 request.POST.getlist('citation_id[]')):
                if not citation_id:
                    citation = Citation(user_id=request.user.id, article=article, text=citation_text,
                                        link=citation_link, date_created=datetime.utcnow().replace(tzinfo=utc),
                                        date_last_edited=datetime.utcnow().replace(tzinfo=utc))
                    citation.save()

        except IntegrityError:
            article_list = Article.objects.filter(topic=topic, deleted=False).order_by('-rating_score')
            form = ArticleForm
            shared_form_pg1 = LinkForm()
            shared_form_pg2 = LinkSummaryForm()
            update_form = ArticleForm(auto_id="id_update_%s")
            update_form.helper = FormHelper()
            update_form.helper.form_tag = False

            context = {
                'topic': topic,
                'article_list': article_list,
                'form': form,
                'error_message': 'Error saving argument. Has this argument name already been used?',
                'shared_form_pg1': shared_form_pg1,
                'shared_form_pg2': shared_form_pg2,
                'update_form': update_form
            }

            return render(request, 'core/article_list.html', context)

        if request.POST.get('_addcitation'):
            return redirect('arguemeter:add-citation-article', article.id)
        else:
            return redirect('arguemeter:list-articles', topic.id)


@login_required
def topic_add_view(request):
    try:
        # communities_moderated = get_objects_for_user(request.user, 'moderate', klass=Community)
        # print(request.POST)
        # approved_communities = []
        # for community_selected in request.POST.get('community'):
        #     community = get_object_or_404(Community, id=community_selected)
        #     if community in communities_moderated:
        #         approved_communities.append(community)
        #         pass
        #     else:
        #         raise PermissionDenied('You are not a moderator for {0}'.format(community.name))

        topic = Topic(user_id=request.user.id, topic_name=request.POST.get('topic_name'),
                      detail=request.POST.get('detail'), date_created=datetime.utcnow().replace(tzinfo=utc),
                      date_last_edited=datetime.utcnow().replace(tzinfo=utc))
        topic.save()

        # for community in approved_communities:
        #     community_tag = CommunityTag(community=community, content_type=topic.content_type, object_id=topic.id)
        #     community_tag.save()
        #     assign_perm('view_topic', community.group, topic)
        #     assign_perm('add_article', community.contributors, topic)

        for rating_id in request.POST.getlist('rating_use[]'):
            rating = Rating.objects.get(pk=rating_id)
            rating.topic.add(topic)
            rating.used_by.add(request.user)
            rating.save()

        for axes_cat_id in request.POST.getlist('axes_use[]'):
            axes_cat = AxesCategorization.objects.get(pk=axes_cat_id)
            axes_cat.topic.add(topic)
            axes_cat.used_by.add(request.user)
            axes_cat.save()

        rating_new_weights = request.POST.getlist('rating_new_weight[]')
        current_weight = 0

        for rating_name in request.POST.getlist('rating_new[]'):

            try:
                rating = Rating.objects.get(name__iexact=rating_name)
            except ObjectDoesNotExist:
                rating = Rating(name=rating_name, weight=rating_new_weights[current_weight])
                rating.save()
            rating.topic.add(topic)
            rating.used_by.add(request.user)
            rating.save()
            current_weight += 1

    except (IntegrityError, ValueError):
        context = construct_topic_list_context(request)
        context.update({'error_message': 'Error saving topic. Has this topic name already been used?'}, )

        return render(request, 'core/topic_list.html', context)

    if request.POST.get('_addcitation'):
        return redirect('arguemeter:add-citation-topic', topic.id)
    else:
        return redirect('arguemeter:index')


@login_required
@is_article_owner
def article_citation_add_view(request, pk):
    article = get_object_or_404(Article, pk=pk)
    context = construct_citation_context(request, article=article)

    if request.method == 'GET':
        return render(request, 'core/citation_form_view.html', context)

    else:
        try:
            if not request.POST.get('_close'):
                citation = Citation(user_id=request.user.id, article=article, text=request.POST.get('text'),
                                    link=request.POST.get('link'), date_created=datetime.utcnow().replace(tzinfo=utc),
                                    date_last_edited=datetime.utcnow().replace(tzinfo=utc))
                citation.save()

        except (IntegrityError, ValueError) as e:
            context.update({'error_message': 'Error saving citation.'},)

            return render(request, 'core/citation_form_view.html', context)

        if request.POST.get('_addanother'):
            return render(request, 'core/citation_form_view.html', context)

        return redirect('arguemeter:list-articles', article.topic_id)


@login_required
@is_topic_owner
def topic_citation_add_view(request, pk):
    topic = get_object_or_404(Topic, pk=pk)
    context = construct_citation_context(request, topic=topic)

    if request.method == 'GET':
        return render(request, 'core/citation_form_view.html', context)

    else:
        try:
            if not request.POST.get('_close'):
                citation = Citation(user_id=request.user.id, topic=topic, text=request.POST.get('text'),
                                    link=request.POST.get('link'), date_created=datetime.utcnow().replace(tzinfo=utc),
                                    date_last_edited=datetime.utcnow().replace(tzinfo=utc))
                citation.save()

        except (IntegrityError, ValueError) as e:
            context.update({'error_message': 'Error saving citation.'},)

            return render(request, 'core/citation_form_view.html', context)

        if request.POST.get('_addanother'):
            return render(request, 'core/citation_form_view.html', context)

        return redirect('arguemeter:index')


@login_required
# @permission_required_or_403('comment', (Article, 'id', 'pk'))
def comment_add_view(request, pk):
    article = get_object_or_404(Article, pk=pk)
    context = construct_article_detail_context(request, article.id)

    try:
        parent = Comment.objects.get(id=request.POST.get('parent'))

        if parent.parent_id is not None:
            context.update({'error_message': 'Error adding comment: only one level of reply nesting allowed.'}, )
            return render(request, 'core/article_detail.html', context)

    except ObjectDoesNotExist:
        parent = None

    try:
        if parent is None:
            comment = Comment(user_id=request.user.id, comment_text=request.POST.get('comment_text'), article=article,
                              date_created=datetime.utcnow().replace(tzinfo=utc),
                              date_last_edited=datetime.utcnow().replace(tzinfo=utc))
        else:
            comment = Comment(user_id=request.user.id, comment_text=request.POST.get('comment_text'), parent=parent,
                              date_created=datetime.utcnow().replace(tzinfo=utc),
                              date_last_edited=datetime.utcnow().replace(tzinfo=utc))
        comment.save()

    except (IntegrityError, ValueError):
        context.update({'error_message': 'Error adding comment.'}, )

        return render(request, 'core/article_detail.html', context)
    return redirect('arguemeter:detail-article', article.id)


@login_required
# @permission_required_or_403('rate_article', (Article, 'id', 'pk'))
@is_not_article_owner
def ratings_add_view(request, pk, context=None):
    article = get_object_or_404(Article, pk=pk)
    if context:
        context.update(construct_article_detail_context(request, article.id))
        return render(request, 'core/article_detail.html', context)
    else:
        context = construct_article_detail_context(request, article.id)
    error = False

    disagree_param = request.POST.get("dis_agree")
    disagree_existed = request.POST.get("disagree_existed")
    disagree_param_bool = disagree_param == 'True'

    if disagree_param and not (disagree_existed == ''):
        try:
            d = DisAgree(article=article, user_id=request.user.id, value=disagree_param_bool, deleted=False,
                         date_created=datetime.utcnow().replace(tzinfo=utc),
                         date_last_edited=datetime.utcnow().replace(tzinfo=utc))
            d.save()
        except IntegrityError:
            context.update({'error_message': 'Error adding agree/disagree'}, )
            error = True
    elif disagree_param and (disagree_existed == ''):
        try:
            d = DisAgree.objects.get(article=article, user_id=request.user.id)
            if d.value != disagree_param_bool:
                d.value = disagree_param_bool
                d.date_last_edited = datetime.utcnow().replace(tzinfo=utc)
                d.save()
        except IntegrityError:
            context.update({'error_message': 'Error updating agree/disagree'}, )
            error = True
    elif disagree_param == '' and disagree_existed == '':
        try:
            d = DisAgree.objects.get(article=article, user_id=request.user.id)
            d.delete()
        except IntegrityError:
            context.update({'error_message': 'Error updating agree/disagree'}, )
            error = True

    rating_list = context.get('rating_list')

    for rating in rating_list:
        rating_param = request.POST.get("rating_{0}".format(rating.id))
        rating_existed = request.POST.get("rating_{0}_existed".format(rating.id)) == 'true'

        if rating_param and (rating_param != '-1') and not rating_existed:
            # create anew
            try:
                rc = RatingCriteria(user_id=request.user.id, rating_id=rating.id, article_id=article.id,
                                    value=rating_param, deleted=False,
                                    date_created=datetime.utcnow().replace(tzinfo=utc),
                                    date_last_edited=datetime.utcnow().replace(tzinfo=utc))
                rc.save()
            except (IntegrityError, ValueError):
                context.update({'error_message': 'Error adding ratings for {0}.'.format(rating.name)}, )
                error = True

        elif rating_param and (rating_param != '-1') and rating_existed:
            # update existing
            rc = RatingCriteria.objects.get(user_id=request.user.id, article_id=article.id, rating_id=rating.id)

            if rc.value != rating_param:
                try:
                    rc.date_last_edited = datetime.utcnow().replace(tzinfo=utc)
                    rc.value = rating_param
                    rc.save()
                except (IntegrityError, ValueError):
                    context.update({'error_message': 'Error updating rating for {0}.'.format(rating.name)}, )
                    error = True

        elif rating_param and (rating_param == '-1') and rating_existed:
            # remove existing
            rc = RatingCriteria.objects.get(user_id=request.user.id, article_id=article.id, rating_id=rating.id)
            try:
                rc.delete()
            except (IntegrityError, ValueError):
                    context.update({'error_message': 'Error removing rating for {0}.'.format(rating.name)}, )
                    error = True

    # axescats = context.get('axescats')
    #
    # for axescat in axescats:
    #     axescat_param = request.POST.get("axescat_{0}".format(axescat.id))
    #     axescat_existed = request.POST.get("axescat_{0}_existed".format(axescat.id))
    #
    #     if axescat_param and (axescat_param != '-1') and not (axescat_existed == ''):
    #         try:
    #             da = DisAgreeAxis(user_id=request.user.id, axescat=axescat,
    #                               slide_value=axescat_param, deleted=False,
    #                               date_created=datetime.utcnow().replace(tzinfo=utc),
    #                               date_last_edited=datetime.utcnow().replace(tzinfo=utc))
    #             da.save()
    #         except (IntegrityError, ValueError) as e:
    #             context.update({'error_message': 'Error adding value for {0}.'.format(axescat.name)},)
    #             error = True
    #
    #     elif axescat_param and (axescat_param != '-1') and (axescat_existed == ''):
    #         da = DisAgreeAxis.objects.get(user_id=request.user.id, axescat=axescat)
    #
    #         if da.slide_value != axescat_param:
    #             try:
    #                 da.date_last_edited = datetime.utcnow().replace(tzinfo=utc)
    #                 da.slide_value = axescat_param
    #                 da.save()
    #             except (IntegrityError, ValueError) as e:
    #                 context.update({'error_message': 'Error updating value for {0}.'.format(axescat.name)},)
    #                 error = True
    #
    if error:
        return render(request, 'core/article_detail.html', context)

    return redirect('arguemeter:detail-article', article.id)


@login_required
@is_topic_owner
def attach_rating(request, pk, rating_id):
    topic = get_object_or_404(Topic, pk=pk)
    rating = get_object_or_404(Rating, pk=rating_id)

    rating.topic.add(topic)
    rating.save()

    return HttpResponse("{}", "application/json")


@login_required
@is_topic_owner
def remove_rating(request, pk, rating_id):
    rating = get_object_or_404(Rating, pk=rating_id)

    rating.topic.remove(pk)
    rating.save()

    return HttpResponse("{}", "application/json")


@login_required
@is_topic_owner
def attach_axescat(request, pk, axescat_id):
    topic = get_object_or_404(Topic, pk=pk)
    axescat = get_object_or_404(AxesCategorization, pk=axescat_id)

    axescat.topic.add(topic)
    axescat.save()

    return HttpResponse("{}", "application/json")


@login_required
@is_topic_owner
def remove_axescat(request, pk, axescat_id):
    axescat = get_object_or_404(AxesCategorization, pk=axescat_id)

    axescat.topic.remove(pk)
    axescat.save()

    return HttpResponse("{}", "application/json")


@login_required
def add_comment_dis_like(request, pk):
    comment = Comment.objects.get(pk=pk)

    remove_comment_dis_like(request, pk)

    if request.GET.get('dis_like') == 'True':
        opinion = 'L'
    elif request.GET.get('dis_like') == 'False':
        opinion = 'D'
    else:
        return HttpResponse("{}", mimetype='application/json')

    dis_like = DisLike(user_id=request.user.id, comment=comment, opinion=opinion)

    dis_like.save()

    return HttpResponse("{}", mimetype='application/json')


# todo: fix permissions checks in a bug story
@login_required
def remove_comment_dis_like(request, pk):
    comment = Comment.objects.get(pk=pk)

    dis_like = DisLike.objects.filter(comment=comment, user_id=request.user.id)

    if dis_like.exists():
        dis_like.get().delete()

    return


def construct_article_detail_context(request, article_id):
    article = get_object_or_404(Article, id=article_id, deleted=False)
    topic = article.topic
    rating_list = Rating.objects.filter(topic=topic, deleted=False).order_by('order')
    # axescats = AxesCategorization.objects.filter(topic=topic, deleted=False)
    tags = ReasonTag.objects.filter(topic=topic, article=article, deleted=False)\
        .annotate(num_agrees=Sum('disagreetag__score'))\
        .order_by('num_agrees')
    comments = Comment.objects.filter(article=article, parent=None, deleted=False).order_by('-date_created')
    featured_comments = sorted(comments, key=lambda a: a.likes)[:3]

    n_shared = Article.objects.filter(topic=topic, deleted=False, topic__deleted=False).exclude(shared_link=None).count()
    n_original = Article.objects.filter(topic=topic, deleted=False, topic__deleted=False, shared_link=None).count()

    try:
        disagree = DisAgree.objects.get(article=article, deleted=False, user=request.user.id)
    except DisAgree.DoesNotExist:
        disagree = None

    dislike = DisLike.objects.filter(deleted=False, user_id=request.user.id, article=article)

    for rating in rating_list:
        if RatingCriteria.objects.filter(deleted=False, user_id=request.user.id, article_id=article.id,
                                         rating_id=rating.id).exists():
            rc = RatingCriteria.objects.get(deleted=False, user_id=request.user.id, article_id=article.id,
                                            rating_id=rating.id)
            rating.user_value = rc.value

    # for axescat in axescats:
    #     if DisAgreeAxis.objects.filter(deleted=False, user_id=request.user.id, axescat_id=axescat.id).exists():
    #         da = DisAgreeAxis.objects.get(deleted=False, user_id=request.user.id, axescat_id=axescat.id)
    #         axescat.user_slide_value = da.slide_value

    for comment in comments:
        if DisLike.objects.filter(user_id=request.user.id, opinion='L', comment=comment).exists():
            comment.user_likes = 'L'
        elif DisLike.objects.filter(user_id=request.user.id, opinion='D', comment=comment).exists():
            comment.user_likes = 'D'

        comment.replies = comment.comment_set.filter()
        for reply in comment.replies:
            if DisLike.objects.filter(user_id=request.user.id, opinion='L', comment=reply).exists():
                reply.user_likes = 'L'
            elif DisLike.objects.filter(user_id=request.user.id, opinion='D', comment=reply).exists():
                reply.user_likes = 'D'

    for tag in tags:
        tag.agrees = DisAgreeTag.objects.filter(tag=tag, article=article, value=True, deleted=False)
        tag.disagrees = DisAgreeTag.objects.filter(tag=tag, article=article, value=False, deleted=False)

        tag.user_dis_agree = None

        if DisAgreeTag.objects.filter(user_id=request.user.id, article=article, tag=tag, value=True, deleted=False).exists():
            tag.user_dis_agree = True
        elif DisAgreeTag.objects.filter(user_id=request.user.id, article=article, tag=tag, value=False, deleted=False).exists():
            tag.user_dis_agree = False

    comment_form = CommentForm()
    update_form = ArticleForm(auto_id="id_update_%s")
    update_form.helper = FormHelper()
    update_form.helper.form_tag = False

    context = {
        'article': article,
        'topic': topic,
        'rating_list': rating_list,
        # 'axescats': axescats,
        'tags': tags,
        'comments': comments,
        'featured_comments': featured_comments,
        'disagree': disagree,
        'dislike': dislike,
        'comment_form': comment_form,
        'update_form': update_form,
        'n_shared': n_shared,
        'n_original': n_original
    }

    return context


def construct_citation_context(request, article=None, topic=None):
    form = CitationForm
    context = {}

    if article is None and topic is not None:
        context.update({
            'topic': topic,
            'form': form,
        })
    elif article is not None and topic is None:
        context.update({
            'article': article,
            'form': form,
        })

    return context


def construct_topic_list_context(request):
    topic_list = Topic.objects.filter(deleted=False).extra(select={
        'n_articles': 'SELECT COUNT(*) ' +
                      'FROM core_article ' +
                      'WHERE core_article.topic_id = ' +
                      'core_topic.id AND ' +
                      "deleted = 'f'"}).extra(
        select={
            'n_user_favorites': "SELECT COUNT(*)" +
                                " FROM core_topicfavorite" +
                                " WHERE core_topicfavorite.topic_id = core_topic.id" +
                                " AND core_topicfavorite.user_id = '" + request.user.id.__str__() +
                                "' AND core_topicfavorite.deleted = '0'"})

    if request.session.get('orderbytopic'):
        orderby = request.session.get('orderbytopic')
    else:
        orderby = '-date_created'

    # apply order by and handle edge cases
    if orderby == '-article_date_last_edited':
        # nullable handling
        topic_list = topic_list.annotate(n_article_date_last_edited=Count('article_date_last_edited'))\
            .order_by('-pinned', '-n_user_favorites', '-n_article_date_last_edited', orderby)
    else:
        topic_list = topic_list.order_by('-pinned', '-n_user_favorites', orderby)

    form = TopicForm()
    update_form = TopicForm(auto_id="id_update_%s")
    default_ratings = Rating.objects.filter(is_default=True, deleted=False)
    custom_ratings = Rating.objects.filter(is_default=False, owner=request.user.id, deleted=False)
    ratings = list(chain(default_ratings, custom_ratings))

    default_axes = AxesCategorization.objects.filter(is_default=True, deleted=False)
    custom_axes = AxesCategorization.objects.filter(is_default=False, user_id=request.user.id, deleted=False)
    axes_cats = list(chain(default_axes, custom_axes))

    # communities = get_objects_for_user(request.user, 'moderate', klass=Community)

    # associate ratings and axes to topics that use
    for topic in topic_list:
        topic.axes_cats = AxesCategorization.objects.filter(topic=topic)
        topic.ratings = Rating.objects.filter(topic=topic).order_by('order')

    context = {
        'topic_list': topic_list,
        'form': form,
        'update_form': update_form,
        'ratings': ratings,
        'axes_cats': axes_cats,
        'default_ratings': default_ratings,
        'custom_ratings': custom_ratings,
        # 'communities': communities
    }
    return context


def assets(request):
    return render(request, 'core/assets.html')


def add_tag_dis_agree(request, article_id, pk):
    tag = get_object_or_404(ReasonTag, id=pk)

    value = ast.literal_eval(request.POST['disagree'])

    article = get_object_or_404(Article, id=article_id)
    existing = DisAgreeTag.objects.filter(tag=tag, user=request.user, article=article, deleted=False)
    existing_value = None

    # delete if already exists

    if existing.exists():
        existing_item = existing.get()
        existing_value = existing_item.value
        existing_item.deleted = True
        existing_item.date_deleted = datetime.now()
        existing_item.save()

    # create if we didn't just delete the same

    if existing_value != value:
        disagree = DisAgreeTag(tag=tag, article=article, value=value, user=request.user)
        disagree.save()

    return redirect('arguemeter:detail-article', article.id)


def delete_article(request, pk):
    try:
        article = get_object_or_404(Article, id=pk)
        article.deleted = True
        article.save()
    except IntegrityError:
        context = construct_article_detail_context(request, pk)
        context.update({'error_message': 'Error deleting article'}, )
        return render(request, 'core/article_detail.html', context)

    return redirect('arguemeter:list-articles', article.topic.id)


def attach_favorite(request, topic_id):
    topic = get_object_or_404(Topic, id=topic_id)
    favorite = TopicFavorite(topic=topic, user=request.user)
    favorite.save()
    return redirect('arguemeter:list-articles', topic.id)


def remove_favorite(request, topic_id):
    topic = get_object_or_404(Topic, id=topic_id)
    favorite = TopicFavorite.objects.get(topic=topic, user=request.user, deleted=False)
    favorite.deleted = True
    favorite.date_deleted = datetime.now()
    favorite.save()
    return redirect('arguemeter:list-articles', topic.id)