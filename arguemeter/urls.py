# Core Django imports
from django.conf.urls import patterns, url, include

# Third party imports
from rest_framework import routers

# App-specific imports
from arguemeter import views
from messages import views as msg_views

__author__ = 'archen'

router = routers.DefaultRouter()
router.register(r'topics', views.TopicViewSet)
router.register(r'article', views.ArticleViewSet)
router.register(r'ratings', views.RatingViewSet)
router.register(r'tags', views.ReasonTagViewSet)
router.register(r'citations', views.CitationViewSet)
router.register(r'inbox', msg_views.InboxViewSet)
router.register(r'messages', msg_views.MessageViewSet)

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),

    # Topic URLs
    url(r'^topics/add/quick/$', views.topic_add_view, name='add-quick-topic'),
    url(r'^topics/(?P<pk>\d+)/edit$', views.TopicUpdate.as_view(), name='edit-topic'),
    url(r'^topics/(?P<pk>\d+)/delete', views.delete_topic, name='delete-topic'),
    url(r'^topics/(?P<pk>\d+)/articles/$', views.topic_articles_list_view, {'context': None}, name='list-articles'),
    url(r'^topics/(?P<pk>\d+)/citations/add/$', views.topic_citation_add_view,
        name='add-citation-topic'),
    url(r'^topics/(?P<pk>\d+)/ratings/(?P<rating_id>\d+)/attach/$', views.attach_rating,
        name='attach-rating'),
    url(r'^topics/(?P<pk>\d+)/ratings/(?P<rating_id>\d+)/remove/$', views.remove_rating,
        name='remove-rating'),
    url(r'^topics/(?P<pk>\d+)/axescats/(?P<axescat_id>\d+)/attach/$', views.attach_axescat,
        name='attach-axescat'),
    url(r'^topics/(?P<pk>\d+)/axescats/(?P<axescat_id>\d+)/remove/$', views.remove_axescat,
        name='remove-axescat'),
    url(r'^topics/(?P<topic_id>\d+)/favorite/attach/$', views.attach_favorite,
        name='attach-favorite'),
    url(r'^topics/(?P<topic_id>\d+)/favorite/remove/$', views.remove_favorite,
        name='remove-favorite'),

    # Article URLs
    url(r'^topics/(?P<pk>\d+)/articles/add/quick/$', views.article_add_view,
        name='add-quick-article'),
    url(r'^articles/(?P<pk>\d+)/edit$', views.ArticleUpdate.as_view(), name='edit-article'),
    url(r'^articles/(?P<pk>\d+)/delete$', views.delete_article, name='delete-article'),
    url(r'^articles/(?P<pk>\d+)$', views.article_detail_view, name='detail-article'),
    url(r'^articles/(?P<pk>\d+)/citations/add/$', views.article_citation_add_view,
        name='add-citation-article'),
    url(r'^articles/(?P<pk>\d+)/comments/add/$', views.comment_add_view,
        name='add-comment'),
    url(r'^articles/(?P<pk>\d+)/ratings/add/$', views.ratings_add_view,
        name='add-ratings'),

    url(r'^articles/(?P<article_id>\d+)/tags/(?P<pk>\d+)/dis_agrees/add/$', views.add_tag_dis_agree, name='add-tag-disagree'),


    url(r'^comments/(?P<pk>\d+)/dis_agrees/add/$', views.add_comment_dis_like,
        name='add-comment-dislike'),
    url(r'^profile/$', views.profile_view, name='view-profile'),
    url(r'^profile/(?P<pk>\d+)/edit/$', views.ProfileUpdate.as_view(), name='edit-profile'),

    url(r'^messages/', msg_views.inbox_all, name='messages'),

    url(r'^api/', include(router.urls)),

    url(r'^assets/$', views.assets)

)

